<section class="content">
  <div class="container">
    <?=$breadcrumb?>
    <div class="space">
     <?php // print_r($this->session->all_userdata());exit; ?>
     <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="box-content">
          <div class="title-case-line">
           <p><?=$title?></p>
         </div>
         
        <div class="table-responsive">
         <div class="card-body">
				<b>Data Pelapor</b><br/><br/>
				<table border="0" class="table table-striped table-hover table-condensed">
					<tr>
						<td width="35%"><b>NIK Pelapor</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['nik_pelapor']; ?></td>
					</tr>
					<tr>
						<td width="35%"><b>NIP Pelapor</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['nip_pelapor']; ?></td>
					</tr>
					<tr>
						<td width="35%"><b>Nama Pelapor</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['nama_pelapor']; ?></td>
					</tr>
					<tr>
						<td width="35%"><b>No Hp Pelapor</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['no_hp_pelapor']; ?></td>
					</tr>
					<tr>
						<td width="35%"><b>Alamat Pelapor</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['alamat_pelapor']; ?></td>
					</tr>
					<tr>
						<td width="35%"><b>Hubungan Dengan Almarhum</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['hubungan_dgn_almarhum']; ?></td>
					</tr>
				</table><br/>
				
				<b>Data Jenazah</b><br/><br/>
				<table border="0" class="table table-striped table-hover table-condensed" >
					<tr>
						<td width="35%"><b>NIK Almarhum</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['nik_almarhum']; ?></td>
					</tr>
					<tr>
						<td><b>Nama Lengkap Almarhum</b></td>
						<td>:</td>
						<td><?php echo $data['nama']; ?></td>
					</tr>
					<tr>
						<td><b>Tanggal Lahir</b></td>
						<td>:</td>
						<td><?php echo $data['tgl_lahir']; ?></td>
					</tr>
					<tr>
						<td><b>Alamat</b></td>
						<td>:</td>
						<td><?php echo $data['alamat']; ?></td>
					</tr>
					<!--tr>
						<td>Waktu Penjemputan</td>
						<td>:</td>
						<td><?php echo $data['alamat']; ?></td>
					</tr-->
					<tr>
						<td><b>Opsi Lain</b></td>
						<td>:</td>
						<td><?php if($data['membawa_keranda']==1){echo "Keranda";}else{echo "Peti";} ?></td>
					</tr>
					<tr>
						<td><b>Keterangan / Catatan untuk penjemputan</b></td>
						<td>:</td>
						<td><?php echo $data['keterangan']; ?></td>
					</tr>
					<tr>
						<td><b>Penjemputan Jenazah</b></td>
						<td>:</td>
						<td><?php echo $data['alamat_awal']; ?></td>
					</tr>
					<tr>
						<td><b>Pengantaran Jenazah</b></td>
						<td>:</td>
						<td><?php echo $data['alamat_tujuan']; ?></td>
					</tr>
					<tr>
						<td><b>Pengantaran Lanjutan</b></td>
						<td>:</td>
						<td><?php echo $data['alamat_lanjutan']; ?></td>
					</tr>
					<tr>
						<td><b>Foto</b></td>
						<td>:</td>
						<td>
							<?php foreach ($foto as $ff){ ?>
							<img class="preview" src="<?php echo base_url($ff['foto']); ?>" alt="Preview Image People" width="25%" />
							<?php } ?>
							<!--img class="preview"  src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" />
							<img class="preview" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" /-->
						</td>
					</tr>
					
				</table><br/>
				
				<b>Data Petugas</b><br/><br/>
				<table border="0" class="table table-striped table-hover table-condensed">
					<tr>
						<td width="35%"><b>Driver</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['driver']; ?></td>
					</tr>
					<tr>
						<td width="35%"><b>Mobil Jenazah</b></td>
						<td width="3%">:</td>
						<td><?php echo $data['jenazah_mobil_plat']; ?></td>
					</tr>
				</table>
		   
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</section>