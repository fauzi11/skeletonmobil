<script>
$(document).ready(function(){
	$("#filtertoggle").click(function(){
		$("#formfilter").slideToggle('slow');
	});
});

$(document).ready(function(){
	$("#filtertoggle2").click(function(){
		$("#formfilter").slideToggle('slow');
	});
});
</script>
<script>
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
<script>
$(function () {
	$(document).ready(function() {
		//$('.tanggal').datepicker({ format : "yyyy m d"});
		//$('.tanggal').datepicker('setDate', 'today');
		// $('.kalender').click(function() {
			 // $(".today").focus();
		// });
	});
});
</script>

<section class="content">
	<div class="container">
		<?=$breadcrumb?>
		<div class="space">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="box-content">
						<div class="title-case-line">
							<div class=" btn-group btn-group-just-ified pull-right">
								<button id="filtertoggle" type="button" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="bottom" title="Filter Pencarian">
									<span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filter
								</button>
								
								<a href="<?php echo base_url('beranda/beranda');?>" class="btn btn-primary red-pink btn-sm" role="button" data-toggle="tooltip" data-placement="bottom" title="Refresh">
									<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset
								</a>
								<a href="<?php echo base_url('beranda/beranda/tambah');?>" class="btn btn-success red-pink btn-sm" role="button" data-toggle="tooltip" data-placement="bottom" title="Tambah Data">
									<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> &nbsp;Add
								</a>
							</div>
							
                            <!--a href="<?php echo  base_url('beranda/beranda/tambah'); ?>" class="btn btn-default btn-sm pull-right" role="button" > 
                            	<span class= "glyphicon glyphicon-plus">&nbsp;Add</span>
                            </a-->
                            <p><?=$title?></p>
                        </div>
                        <?php if (!empty($this->session->flashdata('alert'))) { ?>
                        <?php $alert = $this->session->userdata('alert'); ?>
                        <div class="alert alert-<?=$alert[0] ?>" role="alert"> 
                        	<strong><?=$alert[1] ?></strong>&nbsp;<?=$alert[2] ?>
                        </div>
                        <?php } ?>
                        <div class="table-responsive">

							<!-- filter -->
                        	<form method="get" action="<?php echo base_url('beranda/beranda'); ?>" id="formfilter" class="none">
                        		<div class="col-md-12" >
                        			<div class="col-md-12">
                        				<i class="fa fa-filter"></i> Pilih Data filter
                        				<hr style="margin-top: 2px;">
                        			</div>
                        		</div>
                        		<div class="col-md-12" >
                        			<div class="col-md-4">
                        				<div style="margin-top: -25px;">
                        					<p>
                        						<div class="form-group">
                        							<input type="text" class="form-control" name="cr_nama" value="<?php echo @$cr_nama;?>" placeholder="Nama Almarhum">
                        						</div>
                        					</p>
                        				</div>	
                        			</div>

                        			<div class="col-md-4">
                        				<div style="margin-top: -25px;">
                        					<p>
                        						<div class="form-group">
                        							<input type="text" class="form-control" name="cr_nik" value="<?php echo @$cr_nik;?>" placeholder="NIK Almarhum">
                        						</div>
                        					</p>
                        				</div>	
                        			</div>

                        			<div class="col-md-4">
                        				<div style="margin-top: -25px;">
                        					<p>
                        						<div class="form-group">
                        							<select id="single" name="cr_psn" class="form-control select" style="width:100%" >
                        								<option value="">- Cara Pesan -</option>
                        								<option value="JSS" <?php if(@$cr_psn=='JSS'){echo "selected";} ?>>Jss</option>
                        								<option value="Telfon" <?php if(@$cr_psn=='Telfon'){echo "selected";} ?>>Telfon</option>
                        								<option value="Datang Langsung" <?php if(@$cr_psn=='Datang Langsung'){echo "selected";} ?>>Datang Langsung</option>
                        								
                        							</select>
                        						</div>
                        					</p>
                        				</div>
                        			</div>
									
									<div class="col-md-4">
                        				<div style="margin-top: -10px;">
                        					<p>
                        						<div class="form-group">
                        							<select id="single" name="cr_asal" class="form-control select" style="width:100%" >
                        								<option value="">- Asal Jenazah -</option>
                        								<option value="Warga Kota Yogya" <?php if(@$cr_asal=='Warga Kota Yogya'){echo "selected";} ?>>Warga Kota Yogya</option>
                        								<option value="Bukan Warga Kota Yogya" <?php if(@$cr_asal=='Bukan Warga Kota Yogya'){echo "selected";} ?>>Bukan Warga Kota Yogya</option>
                        							</select>
                        						</div>
                        					</p>
                        				</div>
                        			</div>
									
									<div class="col-md-4">
                        				<div style="margin-top: -10px;">
                        					<p>
                        						<div class="form-group">
                        							<select id="single" name="cr_driv" class="form-control select" style="width:100%" >
                        								<option value="">- Driver -</option>
														<?php foreach($driver as $wtf){?>
                        								<option value="<?=$wtf['jenazah_driver_nama']?>" <?php if(@$cr_driv==$wtf['jenazah_driver_nama']){echo "selected";} ?>><?=$wtf['jenazah_driver_nama']?></option>
                        								<?php } ?>
                        							</select>
                        						</div>
                        					</p>
                        				</div>
                        			</div>
									
									<div class="col-md-2">
										<div style="margin-top:;">
											<input type="text" name="dari" class="form-control pull-right tanggal" placeholder="Tanggal awal">
										</div>
									</div>
									
									<div class="col-md-2">
										<div style="margin-top:;">
											<input type="text" name="ke" class="form-control pull-right tanggal" placeholder="Tanggal akhir">
										</div>
									</div>
									
                        		</div>
					
					<div class="col-md-12">	
						<div class="col-md-2">
							<div style="margin-top: 10px;" class="center">
								<button id="filtertoggle2" type="button" class="btn btn-warning"  data-placement="bottom" style="background:#4DB3A2; border-color:#4DB3A2;">
									<i class="fa fa-close"></i> BATAL
								</button>
							</div>	
						</div>
						<div class="col-md-8"></div>
						<div class="col-md-2 ">
							<div style="margin-top: 10px;" class="">
								<p><button class="btn btn-warning" type="submit" style="background:#8877a9; border-color:#8877a9;"><span class="glyphicon glyphicon-search"></span> Tampilkan</button></p>
							</div>
						</div>
					</div>
				</form>
				<!-- end filter -->
				
				
				<table class="table table-striped table-hover table-bordered table-responsive">
					<!--table class="table table-striped table-hover table-bordered"-->
					<thead>
						<tr>
							<th class="text-center"  rowspan="2" style="width:1%; vertical-align:middle;">NO</th>
							<th class="text-center" style="vertical-align:middle" rowspan="2">Tanggal</th>
							<th class="text-center" rowspan="2" style="vertical-align:middle">Nama Almarhum</th>
							<th class="text-center" colspan="2">Alamat</th>
							<th class="text-center" rowspan="2" style="vertical-align:middle">Keterangan</th>
							<th class="text-center" colspan="2">Jam</th>
							<th class="text-center" rowspan="2" style="vertical-align:middle">Keranda /  Peti</th>
							<th class="text-center" rowspan="2" style="vertical-align:middle">Driver</th>
							<th class="text-center" rowspan="2" style="vertical-align:middle">Mobil</th>
							<th class="text-center" rowspan="2" style="vertical-align:middle">Status</th>
							<th class="text-center" width="18%" rowspan="2" style="vertical-align:middle">Aksi</th>
						</tr>
						<tr>
							<th class="text-center">Rumah Duka / Rumah Sakit</th>
							<th class="text-center">Tujuan / Makam</th>
							<th class="text-center">Order</th>
							<th class="text-center">Layanan</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($datas as $key => $value): ?>
						<tr>
							<td class="text-center"><?=$no++?></td>
							<td><?=date('d F Y', strtotime($value['created_at']));?></td>
							<td><?=$value['nama'];?></td>
							<td><?=$value['alamat_awal'];?></td>
							<td><?=$value['alamat_tujuan'];?></td>
							<td><?=$value['keterangan'];?></td>
							<td><?=date('h : i', strtotime($value['created_at']));?></td>
							<td><?=date('h : i', strtotime($value['waktu_layanan']));?></td>
							<td><?php if($value['membawa_keranda']==1){echo "Keranda";}else{echo "Peti";} ?></td>
							<td><?=$value['driver'];?></td>
							<td><?=$value['jenazah_mobil_plat'];?></td>
							<td class="text-center">
								<span class="label label-<?php 

										switch ($value['status']) {
												case "Baru":
											echo "danger";
												break;
											case "Diproses":
												echo "info";
												break;
											case "Ditolak":
												echo "warning";
												break;
											case "Selesai":
												echo "success";
												break;
											case "Terverifikasi":
												echo "primary";
												break;
										}
									
									?>">
									<?=$value['status'];?>
								</span>
							</td>
							<td class="text-center">
								<div class="btn-gro-up" role="gr-oup">
									<button onclick="driveredit(<?php echo $value['id']; ?>)" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Set Driver">
										<span class="fa fa-id-badge"></span>
									</button> 
									&nbsp;
									
									<button onclick="edit(<?php echo $value['id']; ?>)" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Ubah Status" <?php if($value['status']=="Ditolak" || $value['status']=="Selesai" ){echo "disabled";}?>>
										<span class="glyphicon glyphicon-ok"></span>
									</button> 
									&nbsp;
									<a href="<?php echo base_url('beranda/detail/'.$value['id']); ?>" class="btn btn-xs btn-default" target="_blank" data-toggle="tooltip" data-placement="top" title="Detail">
										<span class="fa fa-search-plus"></span>
									</a>
									&nbsp;
									<a href="<?php echo base_url('beranda/edit/'.$value['id']); ?>" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Data">
										<span class="fa fa-edit"></span>
									</a> 
									&nbsp;
									<a onclick="$('#gotodel').attr('href', '<?php echo base_url('beranda/beranda/hapus/' . $value['id']); ?>')" data-toggle="modal" data-target="#myModal" data-toggle="tooltip" data-placement="top" title="Hapus">
										<span class="fa fa-trash"></span>
									</a>
									 
									
								</div>
							</td>
						</tr>
					<?php endforeach  ?>
				</tbody>
			</table>
			
			<?php if($jml_data != 0){ ?>
			 <div class="row">
            <div class="col-sm-6"><?= $_paging['info']; ?></div>
            <div class="col-sm-6 text-right">
               <ul class="pagination pagination-sm"><?= $_paging['page']; ?></ul>
            </div>
         </div>
			<?php }else{} ?>
			
				<!--div class="col-md-6"style="margin-top:20px">
					<div class="pull-left" >
						
						<?php if($total_data > 0){echo "Menampilkan <b>".$jml_a." - ".$jml_b."</b> dari total <b>".$total_data."</b> data";}else{ echo '<span>Data tidak ditemukan!</span>'; }?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pull-right" style="text-align: right; margin-right: 20px;"> <?php echo $halaman;?></div>
				</div-->
			
		</div>
	</div>
</div>
</div>
</div>
</div>
</section>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Hapus Data Dokumen</h4>
				</div>
				<!-- body modal -->
				<div class="modal-body">
					<p>Anda Yakin Menghapus data ini...?</p>
				</div>
				<!-- footer modal -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<a href="" id="gotodel" type="submit" class="btn btn-danger">Hapus</a>
				</div>
			</div>
	</div>
</div>


<!-- Modal status -->
<div class="modal fade editModal" id="editModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header bg-modal-edit">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-edit"></i> Edit Status Penanganan</h4>
			</div>
			<form class="form-horizontal" id="form_update">
				<div class="modal-body">
                    <div class="form-group mt-20">
                    	<input type="hidden" name="old_id" id="old_id" size="3">
                    	<label class="control-label col-sm-4" for="nama">Pelaporan Status</label>
                    	<div class="col-sm-6">
                    		<select class="form-control" name="new_status">
                    			<option value="Baru">Terverifikasi</option>
                    			<option value="Diproses">Diproses</option>
                    			<option value="Ditolak">Ditolak</option>
                    			<option value="Selesai">Selesai</option>
                    			
                    		</select>
                    	</div>
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-ban"></i> Tutup</button>
					<button type="button" class="btn btn-success btn-sm" id="saveEdit"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class="modal fade" id="driver" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header bg-modal-edit">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-edit"></i> Edit Driver</h4>
			</div>
			<form class="form-horizontal" id="form_update_driver">
				<div class="modal-body">
                    <div class="form-group mt-20">
                    	<input type="hidden" name="old_id" id="old_id" size="3">
                    	<label class="control-label col-sm-4" for="nama">Pilih Driver</label>
                    	<div class="col-sm-6">
                    		<select class="form-control" name="new_driver">
							<?php foreach($driver as $mk){?>
                    			<option value="<?=$mk['jenazah_driver_nama']?>"><?=$mk['jenazah_driver_nama']?></option>
							<?php } ?>
                    		</select>
                    	</div>
                    </div>
					
					<div class="form-group mt-20">
                    	<label class="control-label col-sm-4" for="nama">Pilih Mobil</label>
                    	<div class="col-sm-6">
                    		<select class="form-control" name="new_mobil">
							<?php foreach($mobil as $mbl){?>
                    			<option value="<?=$mbl['jenazah_mobil_id']?>"><?=$mbl['jenazah_mobil_plat']?></option>
							<?php } ?>
                    		</select>
                    	</div>
                    </div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-ban"></i> Tutup</button>
					<button type="button" class="btn btn-success btn-sm" id="savedriver"><i class="fa fa-save"></i> Simpan</button>
				</div>
			</form>
		</div>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

        $('#saveEdit').click(function()
        {
            $.ajax({
                url     : "<?php echo base_url();?>beranda/pelaporan_status",
                type    : "post",
                data    : $("#form_update").serialize(),
                success : function()
                {
                    $('#editModal').modal('hide');
                    location.reload();
                }
            })
        });



	});
	function edit(id)
    {
        $.getJSON('<?php echo base_url();?>beranda/update_status/'+id,
            function(response)
            {
				//alert(id);
                $("#editModal").modal('show');
                $('select[name="new_status"]').val(response['status']);
                $('input[name="old_id"]').val(response['id']);
            }
        );
    }
</script>

<script type="text/javascript">
	$(document).ready(function(){
        $('#savedriver').click(function()
        {
            $.ajax({
                url     : "<?php echo base_url();?>beranda/add_driver",
                type    : "post",
                data    : $("#form_update_driver").serialize(),
                success : function()
                {
                    $('#editModal').modal('hide');
                    location.reload();
                }
            })
        });
	});
	function driveredit(id)
    {
        $.getJSON('<?php echo base_url();?>beranda/update_status/'+id,
            function(response)
            {
				//alert(id);
                $("#driver").modal('show');
                $('select[name="new_driver"]').val(response['driver']);
                $('select[name="new_mobil"]').val(response['mobil']);
                $('input[name="old_id"]').val(response['id']);
            }
        );
    }
</script>