<style type="text/css">
.map{
  height: 350px;
  background: #f7f7f7;
  border-radius: 3px;
}
#selectedFiles img {
  max-width: auto;
  max-height: 170px;
  float: left;
  padding: 5px;
  border-radius: 3px;
  background: #f7f7f7;
  margin:10px;
}

label {
	font-weight:bold;
	}
</style>

<script>
	$(document).ready(function(){
		$("#dgnik").click(function(){
			$("#caridengannik").removeClass('none');
			$("#caridengannip").addClass('none');
			$("#informasipelapor").addClass('none');
			$("#udennik").addClass('none');
			$("#hilang").removeAttr("name");
		});
	});

	$(document).ready(function(){
		$("#dgnip").click(function(){
			$("#caridengannip").removeClass('none');
			$("#caridengannik").addClass('none');
			$("#informasipelapor").addClass('none');
			$("#udennik").addClass('none');
		});
	});

	$(document).ready(function(){
		$("#dgbebas").click(function(){
			$("#caridengannip").addClass('none');
			$("#caridengannik").addClass('none');
			$("#informasipelapor").removeClass('none');
			$("#udennik").removeClass('none');
		});
	});

</script>


<section class="content">
  <div class="container">
    <?=$breadcrumb?>
    <div class="space">
     <?php // print_r($this->session->all_userdata());exit; ?>
     <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="box-content">
          <div class="title-case-line">
           <p><?=$title?></p>
         </div>
         <?php if (!empty($this->session->flashdata('alert'))) { ?>
         <?php $alert = $this->session->userdata('alert'); ?>
         <div class="alert alert-<?=$alert[0] ?>" role="alert"> 
          <strong><?=$alert[1] ?></strong>&nbsp;<?=$alert[2] ?>
        </div>
        <?php } ?>
        <div class="table-responsive">
         <div class="card-body">
           <form class="" method="post" id="myform" enctype="multipart/form-data" action="<?php echo base_url('beranda/beranda/simpan/'.@$result['id']) ?>">
             <input type="hidden" name="action" value="<?php echo $aksi;?>">
             <input type="hidden" name="data[pencatat]" value="<?php echo $this->session->userdata('id_upik');  ?>">
            
             <font color="red"> Data Pelapor </font>
             <hr width="100%">
             
			
			<div class="form-group" width="50%">
        
		<div class="form-group">
		  <label class="radio-inline"><input type="radio" name="agung" id="dgnik">NIK</label>
		  <label class="radio-inline"><input type="radio" name="agung" id="dgnip">NIP</label>
		  <label class="radio-inline"><input type="radio" name="agung" id="dgbebas">Undefined</label>
		</div>

		<div class="none" id="caridengannip">
			<label >NIP Pelapor</label>
			<div class="input-group" >
				
			  <input type="text" class="form-control" name="data[nip_pelapor]" id="nip_pelapor" placeholder="" value="<?php echo @$result['nip_pelapor'];?>">
			  <span class="input-group-btn" onclick="cari2()">
				<button class="btn btn-default btn-group" type="button"><span class="fa fa-search"></span> Cari</button>
			  </span>
			</div>
		</div>
		
		<div class="none" id="caridengannik">
			<label >NIK Pelapor</label>
			<div class="input-group" >
				
			  <input type="text" class="form-control" name="data[nik_pelapor]" id="nik_pelapor" placeholder="" value="<?php echo @$result['nik_pelapor'];?>">
			  <span class="input-group-btn" onclick="cari3()">
				<button class="btn btn-default btn-group" type="button"><span class="fa fa-search"></span> Cari</button>
			  </span>
			</div>
		</div>
		
		<div class="none" id="udennik">
			<div class="form-group">
              <label>NIK Pelapor</label>
              <input type="text" name="data[nik_pelapor]" id="hilang" value="<?php echo @$result['nik_pelapor'];?>" class="form-control" placeholder="" >
            </div>
		</div>
		
        <div class="none" id="informasipelapor">
			<div class="form-group">
              <label>Nama Pelapor</label>
              <input type="text" name="data[nama_pelapor]" value="<?php echo @$result['nama_pelapor'];?>" id="nama_pelapor" class="form-control" placeholder="" >
            </div>
			
           <div class="form-group">
             <label for="exampleInputPassword1">No. Hp</label>
             <input type="text" value="<?php echo @$result['no_hp_pelapor'];?>" class="form-control" placeholder="No. Hp" name="data[no_hp_pelapor]">
           </div>
		   
		    <div class="form-group">
             <label for="exampleInputPassword1">Alamat Pelapor</label>
             <input type="text" value="<?php echo @$result['alamat_pelapor'];?>" id="alamat_pelapor" class="form-control" placeholder="Alamat Pelapor" name="data[alamat_pelapor]">
           </div>
        </div>
           
           <div class="form-group">
            <label>Hubungan dengan Almarhum</label>
            <select class="form-control select2" name="data[hubungan_dgn_almarhum]">
             <option value="">-- Pilih Hubungan --</option>
             <?php foreach ($hubungan as $ldr){ ?>
             <option value="<?php  echo $ldr['hubungan']; ?>" <?php if($ldr['hubungan']==@$result['hubungan_dgn_almarhum']){echo "selected"; } ?>><?php echo $ldr['hubungan']; ?></option>
             <?php } ?>
           </select>
         </div>

         <div class="form-group">
          <label for="exampleInputEmail1">Cara Pesan</label><br/>
		  <label class="radio-inline"><input type="radio" name="data[cara_pesan]" value="Datang Langsung" <?php if(@$result['cara_pesan']=='Datang Langsung'){echo "checked";}?>>Datang Langsung</label>
		<label class="radio-inline"><input type="radio" name="data[cara_pesan]" value="Telfon" <?php if(@$result['cara_pesan']=='Telfon'){echo "checked";}?>>Telfon</label>
       </div>
	   
	   <div class="form-group">
          <label>Asal Jenazah</label><br/>
		  <label class="radio-inline"><input type="radio" name="data[asal_jenazah]" value="Warga Kota Yogya" <?php if(@$result['asal_jenazah']=='Warga Kota Yogya'){echo "checked";}?>>Warga Kota Yogya</label>
		<label class="radio-inline"><input type="radio" name="data[asal_jenazah]" value="Bukan Warga Kota Yogya" <?php if(@$result['asal_jenazah']=='Bukan Warga Kota Yogya'){echo "checked";}?>>Bukan Warga Kota Yogya</label>
       </div>

       <font color="red">Data Jenazah </font>
       <hr width="100%">
       
       <div class="form-group" width="50%">
        <label for="exampleInputEmail1" >NIK Almarhum</label>

        <div class="input-group" >
          <input type="text" class="form-control" name="data[nik_almarhum]" id="nik_almarhum" placeholder="" value="<?php echo @$result['nik_almarhum'];?>">
          <span class="input-group-btn" onclick="cari()">
            <button class="btn btn-default btn-group" type="button"><span class="fa fa-search"></span> Cari</button>
          </span>
        </div>

      </div>

      
      <div class="form-group">
       <label for="exampleInputEmail1">Nama Lengkap Almarhum</label>
       <input type="text" name="data[nama]" value="<?php echo @$result['nama']; ?>" class="form-control" placeholder="Nama Lengkap Almarhum" id="nama_almarhum">
     </div>
     
     <div class="form-group">
       <label>Tanggal Lahir</label>

       <div class="input-group date">
         <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        
        <input type="text" name="data[tgl_lahir]" class="form-control pull-right tanggal" id="tanggal_lahir" value="<?php echo @$result['tgl_lahir']; ?>">
      </div>
      <!-- /.input group -->
    </div>
    
    <div class="form-group">
     <label for="exampleInputEmail1">Alamat</label>
     <input type="text" name="data[alamat]" value="<?php echo @$result['alamat']; ?>" class="form-control" placeholder="Alamat" id="alamat">
   </div>
   
   <font color="red">Pelayanan dan Lokasi </font>
   <hr width="100%">
   
   <div class="form-group">
    <label>Waktu Penjemputan</label>
    <div class="input-group date">
      <div class="input-group-addon">
       <i class="fa fa-calendar"></i>
     </div>
     <input type="text" name="data[waktu_layanan]" value="" class="form-control pull-right tanggal">
   </div>
 </div>
 
 <div class="form-group">
  <label for="exampleInputEmail1">Opsi Lain</label><br/>
  <label class="radio-inline"><input type="radio" name="data[membawa_keranda]" value="0" <?php if(@$result['membawa_keranda']==0){echo "checked";}?>>Keranda</label>
  <label class="radio-inline"><input type="radio" name="data[membawa_keranda]" value="1" <?php if(@$result['membawa_keranda']==1){echo "checked";}?>>Peti</label>
</div>
<br/>

<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#home">Penjemputan Jenazah</a></li>
			<li><a data-toggle="tab" href="#menu1">Pengantaran Jenazah</a></li>
			<li><a data-toggle="tab" href="#menu2">Pengantaran Lanjutan</a></li>
		</ul>
		<div class="tab-content">
		
			<div id="home" class="tab-pane fade in active" width="40%">
				<br/>
				<select type="text" class="form-control oto" id="sl-prop1" name="data[id_prov1]"  style="width:40%;">
					<option value="" selected disabled>Pilihan Provinsi</option>
					<option  value='all'>Semua Provinsi</option>
					<?php foreach ($prov as $pr) { ?>
					<option value="<?=$pr['id'];?>"><?=$pr['name'];?></option>
					<?php } ?>
				</select><br/>
				
				<select type="text" class="form-control oto" id="sl-kab1" name="data[id_kab1]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kabupaten</option>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kecamatan" name="data[id_kec1]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kecamatan</option>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kelurahan" name="data[id_kel1]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kelurahan</option>
				</select><br/>
				
				<input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_awal]" value="<?php echo @$result['alamat_awal']; ?>" placeholder="alamat lengkap"><br/>
				
				<input type="text" class="form-control" style="margin-top:8px;" name="data[ket_alamat_awal]" value="<?php echo @$result['ket_alamat_awal']; ?>" placeholder="keterangan alamat"><br/>
			</div>
			
			<div id="menu1" class="tab-pane fade">
				<br/>
				<select type="text" class="form-control oto" id="sl-prop2" name="data[id_prov2]"  style="width:40%;">
					<option value="" selected disabled>Pilihan Provinsi</option>
					<option  value='all'>Semua Provinsi</option>
					<?php foreach ($prov as $pr) { ?>
					<option value="<?=$pr['id'];?>"><?=$pr['name'];?></option>
					<?php } ?>
				</select><br/>
				
				<select type="text" class="form-control oto" id="sl-kab2" name="data[id_kab2]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kabupaten</option>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kecamatan2" name="data[id_kec2]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kecamatan</option>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kelurahan2" name="data[id_kel2]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kelurahan</option>
				</select><br/>
				
				<input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_tujuan]" id="alamat_penganta-ran" placeholder="alamat lengkap" value="<?php echo @$result['alamat_tujuan']; ?>"><br/>
			</div>
			
			<div id="menu2" class="tab-pane fade">
				<br/>
				<select type="text" class="form-control oto" id="sl-prop3" name="data[id_prov3]"  style="width:40%;">
					<option value="" selected disabled>Pilihan Provinsi</option>
					<option  value='all'>Semua Provinsi</option>
					<?php foreach ($prov as $pr) { ?>
					<option value="<?=$pr['id'];?>"><?=$pr['name'];?></option>
					<?php } ?>
				</select><br/>
				
				<select type="text" class="form-control oto" id="sl-kab3" name="data[id_kab3]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kabupaten</option>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kecamatan3" name="data[id_kec3]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kecamatan</option>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kelurahan3" name="data[id_kel3]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kelurahan</option>
				</select><br/>
				<input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_lanjutan]" id="alamat_la-njutan" value="<?php echo @$result['alamat_lanjutan']; ?>"><br/>
			</div>
		</div>

 <div class="form-group">
     <label for="exampleInputEmail1">Keterangan</label>
	 <textarea class="form-control" rows="5" name="data[keterangan]"><?php echo @$result['keterangan']; ?></textarea>
</div>

<div class="form-group">
  <label>Pilihan Berkas Unggahan</label>
                    <input type="file" class="form-control-file" name="foto[]" onchange="readURLPhoto(this)">

                    <div class="preview">
                     <img class="preview" id="prevImagePhoto" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" />
                   </div>
                 </div>

                 <div class="form-group">
                   <input type="file" class="form-control-file" name="foto[]" onchange="readURLPhoto2(this)">
                   <div class="preview">
                     <img class="preview" id="prevImagePhoto2" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" />
                   </div>
                 </div>

                 <div class="form-group">
                   <input type="file" class="form-control-file" name="foto[]" onchange="readURLPhoto3(this)">
                   <div class="preview">
                     <img class="preview" id="prevImagePhoto3" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" />
                   </div>  </div>

                 </div>

                 
                 <a href="<?php echo base_url('beranda/beranda'); ?>" type="button" class="btn btn-danger pull-right">Batal</a> &nbsp;&nbsp;
                 <button type="submit" class="btn btn-primary pull-right">Submit</button>
               </form>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</section>

<script type="text/javascript">

function cari()
{
  $("#nama_almarhum").val("");
  $("#tanggal_lahir").val("");
  $("#alamat").val("");
  var x = confirm("Apakah NIP yang bersangkutan sudah dalam keadaan meninggal ?");
  if (x){
            //return true;
            var id = $("#nik_almarhum").val();
            $.ajax({
              type    : "POST",
              dataType: 'JSON',
              url     : "<?=base_url('beranda/cari');?>",
              data    : { 'id':id },
              success:function(data){
                if(data['status']==false)
                {
                  alert("Data tidak ditemukan");
                  $("#nama_almarhum").val("");
                  $("#tanggal_lahir").val("");
                  $("#alamat").val("");
                }
                else
                {
                  var obj = JSON.parse(data);
                  $("#nama_almarhum").val(obj['nama']);
                  $("#tanggal_lahir").val(obj['tgllahir']);
                  $("#alamat").val(obj['alamat']);
                }
              },
              error:function(data){
                alert("Gagal mencari data tersebut");
              }
            });
          }
          else{
            return false;
          }
        }
</script>

<script type="text/javascript">
  $(function(){
    $(".datepickereeeeee").datepicker({
     format: 'dd-mm-yyyy',
     autoclose: true,
     todayHighlight: true,
   });
  });

  function readURLPhoto(input) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e)
     {document.getElementById('prevImagePhoto').src=e.target.result;}
     reader.readAsDataURL(input.files[0]);
   }
 }

 function readURLPhoto2(input) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e)
     {document.getElementById('prevImagePhoto2').src=e.target.result;}
     reader.readAsDataURL(input.files[0]);
   }
 }

 function readURLPhoto3(input) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e)
     {document.getElementById('prevImagePhoto3').src=e.target.result;}
     reader.readAsDataURL(input.files[0]);
   }
 }
 </script>
 
<script type="text/javascript">
function cari2()
{
  //$("#nip_pelapor").val("");
  $("#nama_pelapor").val("");
  $("#pogja").val("");
  $("#dinaspelapor").val("");
  //$("#alamat").val("");
  var x = confirm("Apakah NIP yang bersangkutan Pegawai Pemda Kota Jogja ?");
  if (x){
            //return true;
            var id = $("#nip_pelapor").val();
            $.ajax({
              type    : "POST",
              dataType: 'JSON',
              url     : "<?=base_url('beranda/cobaservis');?>",
              data    : { 'id':id },
              success:function(data){
				  //json_decode(data, true);
				  var bunsin= $.parseJSON(data);
				  
                if(bunsin.status==false)
                {
                  alert("Data tidak ditemukan");
                  //$("#nip_pelapor").val("");
                  $("#nama_pelapor").val("");
                  $("#pogja").val("");
                  $("#dinaspelapor").val("");
                }
                else
                {
				$("#informasipelapor").removeClass('none');
                  $("#nama_pelapor").val(bunsin.data[0]['pegawai_nama_lengkap']);
                  $("#pogja").val(bunsin.data[0]['pegawai_pokja_nama_terbaru']);
                  $("#dinaspelapor").val(bunsin.data[0]['pegawai_unit_nama_terbaru']);
                }
              },
              error:function(data){
                alert("Gagal mencari data tersebut");
              }
            });
          }
          else{
            return false;
          }
        }
        </script>
		
<script type="text/javascript">

function cari3()
{
  $("#nama_pelapor").val("");
  $("#alamat_pelapor").val("");
  var x = confirm("Apakah NIK yang bersangkutan warga kota yogya ?");
  if (x){
            //return true;
            var id = $("#nik_pelapor").val();
            $.ajax({
              type    : "POST",
              dataType: 'JSON',
              url     : "<?=base_url('beranda/cari');?>",
              data    : { 'id':id },
              success:function(data){
                if(data['status']==false)
                {
                  alert("Data tidak ditemukan");
                  $("#nama_pelapor").val("");
                  $("#alamat_pelapor").val("");
                }
                else
                {
                  var obj = JSON.parse(data);
				  $("#informasipelapor").removeClass('none');
                  $("#nama_pelapor").val(obj['nama']);
                  $("#alamat_pelapor").val(obj['alamat']);
                }
              },
              error:function(data){
                alert("Gagal mencari data tersebut");
              }
            });
          }
          else{
            return false;
          }
        }
</script>

<script>
	$(document).ready(function () {
        $("#sl-prop1").change(function () {
            var url = "<?php echo site_url('beranda/get_kabupaten'); ?>/" + $(this).val();
            $('#sl-kab1').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#sl-kab1").change(function () {
            var url = "<?php echo site_url('beranda/get_kecamatan'); ?>/" + $(this).val();
            $('#kecamatan').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#kecamatan").change(function () {
            var url = "<?php echo site_url('beranda/get_kelurahan'); ?>/" + $(this).val();
            $('#kelurahan').load(url);
            return false;
        }) 
    });
	
	$(document).ready(function () {
        $("#sl-prop2").change(function () {
            var url = "<?php echo site_url('beranda/get_kabupaten'); ?>/" + $(this).val();
            $('#sl-kab2').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#sl-kab2").change(function () {
            var url = "<?php echo site_url('beranda/get_kecamatan'); ?>/" + $(this).val();
            $('#kecamatan2').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#kecamatan2").change(function () {
            var url = "<?php echo site_url('beranda/get_kelurahan'); ?>/" + $(this).val();
            $('#kelurahan2').load(url);
            return false;
        }) 
    });
	
	//pengantaran lanjutan
    $(document).ready(function () {
        $("#sl-prop3").change(function () {
            var url = "<?php echo site_url('beranda/get_kabupaten'); ?>/" + $(this).val();
            $('#sl-kab3').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#sl-kab3").change(function () {
            var url = "<?php echo site_url('beranda/get_kecamatan'); ?>/" + $(this).val();
            $('#kecamatan3').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#kecamatan3").change(function () {
            var url = "<?php echo site_url('beranda/get_kelurahan'); ?>/" + $(this).val();
            $('#kelurahan3').load(url);
            return false;
        }) 
    });
	
</script>