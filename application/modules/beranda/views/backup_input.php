<style type="text/css">
.map{
  height: 350px;
  background: #f7f7f7;
  border-radius: 3px;
}
#selectedFiles img {
  max-width: auto;
  max-height: 170px;
  float: left;
  padding: 5px;
  border-radius: 3px;
  background: #f7f7f7;
  margin:10px;
}

label {
	font-weight:bold;
	}
</style>

<script>
$(document).ready(function(){
	$("#dgnik").click(function(){
		$("#caridengannik").removeClass('none');
		$("#caridengannip").addClass('none');
		$("#informasipelapor").addClass('none');
		$("#udennik").addClass('none');
		$("#hilang").remove();
	});
});

$(document).ready(function(){
	$("#dgnip").click(function(){
		$("#caridengannip").removeClass('none');
		$("#caridengannik").addClass('none');
		$("#informasipelapor").addClass('none');
		$("#udennik").addClass('none');
	});
});

$(document).ready(function(){
	$("#dgbebas").click(function(){
		$("#caridengannip").addClass('none');
		$("#caridengannik").addClass('none');
		$("#informasipelapor").removeClass('none');
		$("#udennik").removeClass('none');
	});
});
</script>


<section class="content">
  <div class="container">
    <?=$breadcrumb?>
    <div class="space">
     <?php // print_r($this->session->all_userdata());exit; ?>
     <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="box-content">
          <div class="title-case-line">
           <p><?=$title?></p>
         </div>
         <?php if (!empty($this->session->flashdata('alert'))) { ?>
         <?php $alert = $this->session->userdata('alert'); ?>
         <div class="alert alert-<?=$alert[0] ?>" role="alert"> 
          <strong><?=$alert[1] ?></strong>&nbsp;<?=$alert[2] ?>
        </div>
        <?php } ?>
        <div class="table-responsive">
         <div class="card-body">
           <form class="" method="post" id="myform" enctype="multipart/form-data" action="<?php echo base_url('beranda/beranda/simpan/'.@$result['id']) ?>">
             <input type="hidden" name="action" value="<?php echo $aksi;?>">
             <input type="hidden" name="data[pencatat]" value="<?php echo $this->session->userdata('id_upik');  ?>">
            
             <font color="red"> Data Pelapor </font>
             <hr width="100%">
             
			
			<div class="form-group" width="50%">
        
		<div class="form-group">
		  <label class="radio-inline"><input type="radio" name="agung" id="dgnik">NIK</label>
		  <label class="radio-inline"><input type="radio" name="agung" id="dgnip">NIP</label>
		  <label class="radio-inline"><input type="radio" name="agung" id="dgbebas">Undefined</label>
		</div>

		<div class="none" id="caridengannip">
			<label >NIP Pelapor</label>
			<div class="input-group" >
				
			  <input type="text" class="form-control" name="data[nip_pelapor]" id="nip_pelapor" placeholder="" value="<?php echo @$result['nip_pelapor'];?>">
			  <span class="input-group-btn" onclick="cari2()">
				<button class="btn btn-default btn-group" type="button"><span class="fa fa-search"></span> Cari</button>
			  </span>
			</div>
		</div>
		
		<div class="none" id="caridengannik">
			<label >NIK Pelapor</label>
			<div class="input-group" >
				
			  <input type="text" class="form-control" name="data[nik_pelapor]" id="nik_pelapor" placeholder="" value="<?php echo @$result['nik_pelapor'];?>">
			  <span class="input-group-btn" onclick="cari3()">
				<button class="btn btn-default btn-group" type="button"><span class="fa fa-search"></span> Cari</button>
			  </span>
			</div>
		</div>
		
		<div class="none" id="udennik">
			<div class="form-group">
              <label>NIK Pelapor</label>
              <input type="text" name="data[nik_pelapor]" id="hilang" value="<?php echo @$result['nik_pelapor'];?>" class="form-control" placeholder="" >
            </div>
		</div>
		
        <div class="none" id="informasipelapor">
			<div class="form-group">
              <label>Nama Pelapor</label>
              <input type="text" name="data[nama_pelapor]" value="<?php echo @$result['nama_pelapor'];?>" id="nama_pelapor" class="form-control" placeholder="" >
            </div>
			
           <div class="form-group">
             <label for="exampleInputPassword1">No. Hp</label>
             <input type="text" value="<?php echo @$result['no_hp_pelapor'];?>" class="form-control" placeholder="No. Hp" name="data[no_hp_pelapor]">
           </div>
		   
		    <div class="form-group">
             <label for="exampleInputPassword1">Alamat Pelapor</label>
             <input type="text" value="<?php echo @$result['alamat_pelapor'];?>" id="alamat_pelapor" class="form-control" placeholder="Alamat Pelapor" name="data[alamat_pelapor]">
           </div>
        </div>
           
           <div class="form-group">
            <label>Hubungan dengan Almarhum</label>
            <select class="form-control select2" name="data[hubungan_dgn_almarhum]">
             <option value="">-- Pilih Hubungan --</option>
             <?php foreach ($hubungan as $ldr){ ?>
             <option value="<?php  echo $ldr['hubungan']; ?>" <?php if($ldr['hubungan']==@$result['hubungan_dgn_almarhum']){echo "selected"; } ?>><?php echo $ldr['hubungan']; ?></option>
             <?php } ?>
           </select>
         </div>

         <div class="form-group">
          <label for="exampleInputEmail1">Cara Pesan</label><br/>
		  <label class="radio-inline"><input type="radio" name="data[cara_pesan]" value="Datang Langsung" <?php if(@$result['cara_pesan']=='Datang Langsung'){echo "checked";}?>>Datang Langsung</label>
		<label class="radio-inline"><input type="radio" name="data[cara_pesan]" value="Telfon" <?php if(@$result['cara_pesan']=='Telfon'){echo "checked";}?>>Telfon</label>
		  
          <!--select class="form-control select2" name="data[cara_pesan]" required>
           <option value="">-- Pilih Pemesanan --</option>
           <option value="Datang Langsung" <?php if(@$result['cara_pesan']=="Datang Langsung"){echo "selected"; } ?>>Datang Langsung</option>
           <option value="Telfon" <?php if(@$result['cara_pesan']=="Telfon"){echo "selected"; } ?>>Telfon</option>
         </select-->
       </div>
	   
	   <div class="form-group">
          <label>Asal Jenazah</label><br/>
		  <label class="radio-inline"><input type="radio" name="data[asal_jenazah]" value="Warga Kota Yogya" <?php if(@$result['asal_jenazah']=='Warga Kota Yogya'){echo "checked";}?>>Warga Kota Yogya</label>
		<label class="radio-inline"><input type="radio" name="data[asal_jenazah]" value="Bukan Warga Kota Yogya" <?php if(@$result['asal_jenazah']=='Bukan Warga Kota Yogya'){echo "checked";}?>>Bukan Warga Kota Yogya</label>
		  
          <!--select class="form-control select2" name="data[asal_jenazah]" required>
           <option value="">-- Pilih Asal Jenazah --</option>
           <option value="Warga Kota Yogya" <?php if(@$result['asal_jenazah']=="Warga Kota Yogya"){echo "selected"; } ?>>Warga Kota Yogya</option>
           <option value="Bukan Warga Kota Yogya" <?php if(@$result['asal_jenazah']=="Bukan Warga Kota Yogya"){echo "selected"; } ?>>Bukan Warga Kota Yogya</option>
         </select-->
       </div>

       <font color="red">Data Jenazah </font>
       <hr width="100%">
       
       <div class="form-group" width="50%">
        <label for="exampleInputEmail1" >NIK Almarhum</label>

        <div class="input-group" >
          <input type="text" class="form-control" name="data[nik_almarhum]" id="nik_almarhum" placeholder="" value="<?php echo @$result['nik_almarhum'];?>">
          <span class="input-group-btn" onclick="cari()">
            <button class="btn btn-default btn-group" type="button"><span class="fa fa-search"></span> Cari</button>
          </span>
        </div>

      </div>

      
      <div class="form-group">
       <label for="exampleInputEmail1">Nama Lengkap Almarhum</label>
       <input type="text" name="data[nama]" value="<?php echo @$result['nama']; ?>" class="form-control" placeholder="Nama Lengkap Almarhum" id="nama_almarhum">
     </div>
     
     <div class="form-group">
       <label>Tanggal Lahir</label>

       <div class="input-group date">
         <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        
        <input type="text" name="data[tgl_lahir]" class="form-control pull-right tanggal" id="tanggal_lahir" value="<?php echo @$result['tgl_lahir']; ?>">
      </div>
      <!-- /.input group -->
    </div>
    
    <div class="form-group">
     <label for="exampleInputEmail1">Alamat</label>
     <input type="text" name="data[alamat]" value="<?php echo @$result['alamat']; ?>" class="form-control" placeholder="Alamat" id="alamat">
   </div>
   
   <font color="red">Pelayanan dan Lokasi </font>
   <hr width="100%">
   
   <div class="form-group">
    <label>Waktu Penjemputan</label>
    <div class="input-group date">
      <div class="input-group-addon">
       <i class="fa fa-calendar"></i>
     </div>
     <input type="text" name="data[waktu_layanan]" value="" class="form-control pull-right tanggal">
   </div>
 </div>
 
 <div class="form-group">
  <label for="exampleInputEmail1">Opsi Lain</label><br/>
  <label class="radio-inline"><input type="radio" name="data[membawa_keranda]" value="0" <?php if(@$result['membawa_keranda']==0){echo "checked";}?>>Keranda</label>
  <label class="radio-inline"><input type="radio" name="data[membawa_keranda]" value="1" <?php if(@$result['membawa_keranda']==1){echo "checked";}?>>Peti</label>
</div>
<br/>

<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#home">Penjemputan Jenazah</a></li>
			<li><a data-toggle="tab" href="#menu1">Pengantaran Jenazah</a></li>
			<li><a data-toggle="tab" href="#menu2">Pengantaran Lanjutan</a></li>
		</ul>
		<div class="tab-content">
		
			<div id="home" class="tab-pane fade in active" width="40%">
				<br/>
				<select type="text" class="form-control oto" id="sl-prop1" name="data[id_prov1]"  style="width:40%;">
					<option value="" selected disabled>Pilihan Provinsi</option>
					<option  value='all'>Semua Provinsi</option>
					<?php foreach ($prov as $pr) { ?>
					<option value="<?=$pr['id'];?>"><?=$pr['name'];?></option>
					<?php } ?>
				</select><br/>
				
				<select type="text" class="form-control oto" id="sl-kab1" name="data[id_kab1]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kabupaten</option>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kecamatan" name="data[id_kec1]"  style="width:40%;">
					<option value="" selected disabled>Pilihan Kecamatan</option>
					<option  value='all'>Semua Kecamatan</option>
					<?php foreach ($ref_kecamatan as $key) { ?>
					<option value="<?=$key['id'];?>"><?=$key['kecamatan'];?></option>
					<?php } ?>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kelurahan" name="data[id_kel1]" style="width:40%;">
					<option value="" selected disabled>Pilihan Kelurahan</option>
				</select><br/>
				
				<input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_awal]" id="alamat_penj-emputan" value="<?php echo @$result['alamat_awal']; ?>"><br/>
			</div>
			
			<div id="menu1" class="tab-pane fade">
				<br/>
				<select type="text" class="form-control oto" id="kecamatan2" name="kecamatan" onchange="cari_kelurahan2()">
					<option value="" selected disabled>Pilihan Kecamatan</option>
					<option  value='all'>Semua Kecamatan</option>
					<?php foreach ($ref_kecamatan as $key) { ?>
					<option value="<?=$key['id'];?>"><?=$key['kecamatan'];?></option>
					<?php } ?>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kelurahan2" name="kelurahan">
					<option value="" selected disabled>Pilihan Kelurahan</option>
				</select><br/>
				<input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_tujuan]" id="alamat_penganta-ran" value="<?php echo @$result['alamat_tujuan']; ?>"><br/>
			</div>
			
			
			
			<div id="menu2" class="tab-pane fade">
				<br/>
				<select type="text" class="form-control oto" id="kecamatan3" name="kecamatan" onchange="cari_kelurahan3()">
					<option value="" selected disabled>Pilihan Kecamatan</option>
					<option  value='all'>Semua Kecamatan</option>
					<?php foreach ($ref_kecamatan as $key) { ?>
					<option value="<?=$key['id'];?>"><?=$key['kecamatan'];?></option>
					<?php } ?>
				</select><br/>
				
				<select type="text" class="form-control oto" id="kelurahan3" name="kelurahan">
					<option value="" selected disabled>Pilihan Kelurahan</option>
				</select><br/>
				<input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_lanjutan]" id="alamat_la-njutan" value="<?php echo @$result['alamat_lanjutan']; ?>"><br/>
			</div>
		</div>


<!--div class="form-group">
  <label>Penjemputan Jenazah</label>
  <select type="text" class="form-control oto" id="kecamatan" name="kecamatan" onchange="cari_kelurahan()">
		<option value="" selected disabled>Pilihan Kecamatan</option>
		<option  value='all'>Semua Kecamatan</option>
		<?php foreach ($ref_kecamatan as $key) { ?>
		<option value="<?=$key['id'];?>"><?=$key['kecamatan'];?></option>
		<?php } ?>
	</select><br/>
	
	<select type="text" class="form-control oto" id="kelurahan" name="kelurahan">
		<option value="" selected disabled>Pilihan Kelurahan</option>
	</select><br/>
  
  <!--div class="map" id="googleMap"></div>
  <input type="hidden" class="form-control" name="data[lat_awal]" id="lat_awal" value="<?php echo @$result['lat_awal']; ?>" readonly>
  <input type="hidden" class="form-control" name="data[lng_awal]" id="lng_awal" value="<?php echo @$result['lng_awal']; ?>" readonly>
  <input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_awal]" id="alamat_penj-emputan" value="<?php echo @$result['alamat_awal']; ?>" >
</div-->

<!--div class="form-group">
  <label>Pengantaran Jenazah</label>
	
	<select type="text" class="form-control oto" id="kecamatan2" name="kecamatan" onchange="cari_kelurahan2()">
		<option value="" selected disabled>Pilihan Kecamatan</option>
		<option  value='all'>Semua Kecamatan</option>
		<?php foreach ($ref_kecamatan as $key) { ?>
		<option value="<?=$key['id'];?>"><?=$key['kecamatan'];?></option>
		<?php } ?>
	</select><br/>
	
	<select type="text" class="form-control oto" id="kelurahan2" name="kelurahan">
		<option value="" selected disabled>Pilihan Kelurahan</option>
	</select><br/>
  
  <!--div class="map" id="googleMap2"></div>
  <input type="hidden" class="form-control" name="data[lat_tujuan]" id="lat_tujuan" value="<?php echo @$result['lat_tujuan']; ?>" readonly>
  <input type="hidden" class="form-control" name="data[lng_tujuan]" id="lng_tujuan" value="<?php echo @$result['lng_tujuan']; ?>" readonly>
  <input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_tujuan]" id="alamat_penganta-ran" value="<?php echo @$result['alamat_tujuan']; ?>">
</div-->

<!--div class="form-group">
  <label>Pengantaran Lanjutan</label>
  
  <select type="text" class="form-control oto" id="kecamatan3" name="kecamatan" onchange="cari_kelurahan3()">
		<option value="" selected disabled>Pilihan Kecamatan</option>
		<option  value='all'>Semua Kecamatan</option>
		<?php foreach ($ref_kecamatan as $key) { ?>
		<option value="<?=$key['id'];?>"><?=$key['kecamatan'];?></option>
		<?php } ?>
	</select><br/>
	
	<select type="text" class="form-control oto" id="kelurahan3" name="kelurahan">
		<option value="" selected disabled>Pilihan Kelurahan</option>
	</select><br/>
  <!--div class="map" id="googleMap3"></div>
  <input type="hidden" class="form-control" name="data[lat_lanjutan]" id="lat_lanjutan" value="<?php echo @$result['lat_lanjutan']; ?>" readonly>
  <input type="hidden" class="form-control" name="data[lng_lanjutan]" id="lng_lanjutan" value="<?php echo @$result['lng_lanjutan']; ?>" readonly>
  <input type="text" class="form-control" style="margin-top:8px;" name="data[alamat_lanjutan]" id="alamat_la-njutan" value="<?php echo @$result['alamat_lanjutan']; ?>">
</div-->

 <div class="form-group">
     <label for="exampleInputEmail1">Keterangan</label>
     <!--input type="text" name="data[alamat]" value="<?php echo @$result['alamat']; ?>" class="form-control" placeholder="Alamat" id="alamat"-->
	 <textarea class="form-control" rows="5" name="data[keterangan]"><?php echo @$result['keterangan']; ?></textarea>
</div>

<div class="form-group">
  <label>Pilihan Berkas Unggahan</label>
										<!--input type="file" class="form-control" name="foto[]" id="file" placeholder="Klik utk memilih" multiple>
                    <input type="file" class="form-control" name="foto[]" id="file" placeholder="Klik utk memilih" multiple-->
                    <input type="file" class="form-control-file" name="foto[]" onchange="readURLPhoto(this)">


                    <div class="preview">
                     <img class="preview" id="prevImagePhoto" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" />
                   </div>
                 </div>


                 <div class="form-group">
                   <input type="file" class="form-control-file" name="foto[]" onchange="readURLPhoto2(this)">
                   <div class="preview">
                     <img class="preview" id="prevImagePhoto2" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" />
                   </div>
                 </div>


                 <div class="form-group">
                   <input type="file" class="form-control-file" name="foto[]" onchange="readURLPhoto3(this)">
                   <div class="preview">
                     <img class="preview" id="prevImagePhoto3" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" width="25%" />
                   </div>  </div>

                 </div>


                 
                 <a href="<?php echo base_url('beranda/beranda'); ?>" type="button" class="btn btn-danger pull-right">Batal</a> &nbsp;&nbsp;
                 <button type="submit" class="btn btn-primary pull-right">Submit</button>
               </form>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</section>

<script type="text/javascript">

function cari()
{
  $("#nama_almarhum").val("");
  $("#tanggal_lahir").val("");
  $("#alamat").val("");
  var x = confirm("Apakah NIP yang bersangkutan sudah dalam keadaan meninggal ?");
  if (x){
            //return true;
            var id = $("#nik_almarhum").val();
            $.ajax({
              type    : "POST",
              dataType: 'JSON',
              url     : "<?=base_url('beranda/cari');?>",
              data    : { 'id':id },
              success:function(data){
                if(data['status']==false)
                {
                  alert("Data tidak ditemukan");
                  $("#nama_almarhum").val("");
                  $("#tanggal_lahir").val("");
                  $("#alamat").val("");
                }
                else
                {
                  var obj = JSON.parse(data);
                  $("#nama_almarhum").val(obj['nama']);
                  $("#tanggal_lahir").val(obj['tgllahir']);
                  $("#alamat").val(obj['alamat']);
                }
              },
              error:function(data){
                alert("Gagal mencari data tersebut");
              }
            });
          }
          else{
            return false;
          }
        }
        </script>


        <script type="text/javascript">
        var map,map2,map3;

        var markers = [];

        function initMap() {
    // Multiple Markers
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;
    var url;
    var pemkot = {lat: -7.800110405480062, lng: 110.39114728783568};

    var styles = {
      default: null,
      hide: [
      {
        featureType: 'poi.business',
        stylers: [{visibility: 'on'}]
      },
      {
        featureType: 'transit',
        stylers: [{visibility: 'on'}]
      },
      {
        elementType: 'labels.icon',
        stylers: [{visibility: 'off'}]
      },
      ]
    };


    map = new google.maps.Map(document.getElementById('googleMap'), {
      zoom: 13,
      center: pemkot,
      styles: styles['hide']
    });

    map2 = new google.maps.Map(document.getElementById('googleMap2'), {
      zoom: 13,
      center: pemkot,
      styles: styles['hide']
    });

    map3 = new google.maps.Map(document.getElementById('googleMap3'), {
      zoom: 13,
      center: pemkot,
      styles: styles['hide']
    });

    var marker = new google.maps.Marker({
      position: pemkot,
      map: map,
      title: 'Lokasi Penjemputan',
      draggable: true
    });

    var marker2 = new google.maps.Marker({
      position: pemkot,
      map: map2,
      title: 'Lokasi Pengantaran',
      draggable: true
    });

    var marker3 = new google.maps.Marker({
      position: pemkot,
      map: map3,
      title: 'Lokasi Lanjutan',
      draggable: true
    });

    //
    geocodeLatLng(geocoder, map, infowindow, "-7.800110405480062", "110.39114728783568", "#alamat_penjemputan");
    geocodeLatLng(geocoder, map2, infowindow, "-7.800110405480062", "110.39114728783568", "#alamat_pengantaran");
    geocodeLatLng(geocoder, map3, infowindow, "-7.800110405480062", "110.39114728783568", "#alamat_lanjutan");
    //
    google.maps.event.addListener(marker, 'dragend', function(event) {
        //alert("Lokasi Penjemputan telah ditentukan : "+this.getPosition().lat()+" dan "+this.getPosition().lng());
        geocodeLatLng(geocoder, map, infowindow, this.getPosition().lat(), this.getPosition().lng(), "#alamat_penjemputan");
        $("#lat_awal").val(this.getPosition().lat());
        $("#lng_awal").val(this.getPosition().lng());
      });

    google.maps.event.addListener(marker2, 'dragend', function(event) {
        //alert("Lokasi Pengantaran telah ditentukan : "+this.getPosition().lat()+" dan "+this.getPosition().lng());
        geocodeLatLng(geocoder, map2, infowindow, this.getPosition().lat(), this.getPosition().lng(), "#alamat_pengantaran");
        $("#lat_tujuan").val(this.getPosition().lat());
        $("#lng_tujuan").val(this.getPosition().lng());
      });

    google.maps.event.addListener(marker3, 'dragend', function(event) {
        //alert("Lokasi Lanjutan telah ditentukan : "+this.getPosition().lat()+" dan "+this.getPosition().lng());
        geocodeLatLng(geocoder, map3, infowindow, this.getPosition().lat(), this.getPosition().lng(), "#alamat_lanjutan");
        $("#lat_lanjutan").val(this.getPosition().lat());
        $("#lng_lanjutan").val(this.getPosition().lng());
      });
  }

  function geocodeLatLng(geocoder, map, infowindow, lat, lng, alamat) {
    var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK')
      {
        if (results[0])
        {
                /*map.setZoom(11);
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                  });*/
                //infowindow.setContent(results[0].formatted_address);
                //infowindow.open(map, marker);
                $(alamat).val(results[0].formatted_address);
              } 
              else 
              {
                window.alert('No results found');
              }
            }
            else 
            {
              window.alert('Geocoder failed due to: ' + status);
            }
          });
  }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVHpUSMf6VsoMPEiNIoTOlLIR48r7qSs8&callback=initMap"></script>


  <script type="text/javascript">
  $(function(){
    $(".datepickereeeeee").datepicker({
     format: 'dd-mm-yyyy',
     autoclose: true,
     todayHighlight: true,
   });
  });

  function readURLPhoto(input) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e)
     {document.getElementById('prevImagePhoto').src=e.target.result;}
     reader.readAsDataURL(input.files[0]);
   }
 }

 function readURLPhoto2(input) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e)
     {document.getElementById('prevImagePhoto2').src=e.target.result;}
     reader.readAsDataURL(input.files[0]);
   }
 }

 function readURLPhoto3(input) {
   if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e)
     {document.getElementById('prevImagePhoto3').src=e.target.result;}
     reader.readAsDataURL(input.files[0]);
   }
 }
 </script>
 
 <script type="text/javascript">

function cari2()
{
  //$("#nip_pelapor").val("");
  $("#nama_pelapor").val("");
  $("#pogja").val("");
  $("#dinaspelapor").val("");
  //$("#alamat").val("");
  var x = confirm("Apakah NIP yang bersangkutan Pegawai Pemda Kota Jogja ?");
  if (x){
            //return true;
            var id = $("#nip_pelapor").val();
            $.ajax({
              type    : "POST",
              dataType: 'JSON',
              url     : "<?=base_url('beranda/cobaservis');?>",
              data    : { 'id':id },
              success:function(data){
				  //json_decode(data, true);
				  var bunsin= $.parseJSON(data);
				  
                if(bunsin.status==false)
                {
                  alert("Data tidak ditemukan");
                  //$("#nip_pelapor").val("");
                  $("#nama_pelapor").val("");
                  $("#pogja").val("");
                  $("#dinaspelapor").val("");
                }
                else
                {
					//alert("Data bajilak temukan");
					//console.log(bunsin.data[0]['pegawai_nama_lengkap'])
					//alert(bunsin.pegawai_nama_lengkap);
                 
                 // $("#nip_pelapor").val(obj['nama']);
				$("#informasipelapor").removeClass('none');
                  $("#nama_pelapor").val(bunsin.data[0]['pegawai_nama_lengkap']);
                  $("#pogja").val(bunsin.data[0]['pegawai_pokja_nama_terbaru']);
                  $("#dinaspelapor").val(bunsin.data[0]['pegawai_unit_nama_terbaru']);
				  //console.log(obj['pegawai_nama_lengkap'])
                }
              },
              error:function(data){
                alert("Gagal mencari data tersebut");
              }
            });
          }
          else{
            return false;
          }
        }
        </script>
		
<script type="text/javascript">

function cari3()
{
  $("#nama_pelapor").val("");
  $("#alamat_pelapor").val("");
  var x = confirm("Apakah NIK yang bersangkutan warga kota yogya ?");
  if (x){
            //return true;
            var id = $("#nik_pelapor").val();
            $.ajax({
              type    : "POST",
              dataType: 'JSON',
              url     : "<?=base_url('beranda/cari');?>",
              data    : { 'id':id },
              success:function(data){
                if(data['status']==false)
                {
                  alert("Data tidak ditemukan");
                  $("#nama_pelapor").val("");
                  $("#alamat_pelapor").val("");
                }
                else
                {
                  var obj = JSON.parse(data);
				  $("#informasipelapor").removeClass('none');
                  $("#nama_pelapor").val(obj['nama']);
                  $("#alamat_pelapor").val(obj['alamat']);
                }
              },
              error:function(data){
                alert("Gagal mencari data tersebut");
              }
            });
          }
          else{
            return false;
          }
        }
</script>

<script>
	$(document).ready(function () {
        $("#sl-prop1").change(function () {
            var url = "<?php echo site_url('beranda/get_kabupaten'); ?>/" + $(this).val();
            $('#sl-kab1').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#sl-kab1").change(function () {
            var url = "<?php echo site_url('beranda/get_kecamatan'); ?>/" + $(this).val();
            $('#kecamatan').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#kecamatan").change(function () {
            var url = "<?php echo site_url('beranda/get_kelurahan'); ?>/" + $(this).val();
            $('#kelurahan').load(url);
            return false;
        }) 
    });
	

    
	$(document).ready(function () {
        $("#kecamatan2").change(function () {
            var url = "<?php echo site_url('beranda/ref_kelurahan'); ?>/" + $(this).val();
            $('#kelurahan2').load(url);
            return false;
        }) 
    });
	$(document).ready(function () {
        $("#kecamatan3").change(function () {
            var url = "<?php echo site_url('beranda/ref_kelurahan'); ?>/" + $(this).val();
            $('#kelurahan3').load(url);
            return false;
        }) 
    });
</script>