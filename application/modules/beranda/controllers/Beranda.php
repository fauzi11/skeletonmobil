<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Beranda extends MX_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('mm'));
        //$this->autentication->authorize();
        $this->load->library(array('pagination','upload','layouts'));
        $this->load->helper(array('url','html','form'));
        $this->load->module('theme');
    }
    
	public $_ctrl  = 'beranda';
	public $_kunci = 'id';
     
    public function index($offset = null)
    {
        $load['title'] = 'Beranda';
        $breadcrumb    = array(
            array(
                'name' => 'Home',
                'icon' => 'icon-home',
                'url' => ''
            ),
            array(
                'name' => 'Beranda',
                'icon' => 'fa fa-alarm'
            )
        );
        
        $load['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
       
        //load data
        $where = array();
        $like  = array();
		
        if (isset($_GET['cr_nama']) AND $_GET['cr_nama']) {
            $cr_nama = $_GET['cr_nama'];
            if ($cr_nama)
                $like[] = array(
                    'tbl_jenazah_layanan.nama',
                    $cr_nama
                );
            $load['cr_nama'] = $cr_nama;
        }
       
        if (isset($_GET['cr_nik']) AND $_GET['cr_nik']) {
            $cr_nik = $_GET['cr_nik'];
            if ($cr_nik)
                $like[] = array(
                    'tbl_jenazah_layanan.nik_almarhum',
                    $cr_nik
                );
            $load['cr_nik'] = $cr_nik;
        }
		
		if(isset($_GET['cr_psn']) AND $_GET['cr_psn']){
			$cr_psn = $_GET['cr_psn'];
			if($cr_psn)
				$like[] = array('tbl_jenazah_layanan.cara_pesan', $cr_psn);
				$load['cr_psn']=$cr_psn;
		}
		
		if(isset($_GET['cr_asal']) AND $_GET['cr_asal']){
			$cr_asal = $_GET['cr_asal'];
			if($cr_asal)
				$like[] = array('tbl_jenazah_layanan.asal_jenazah', $cr_asal);
				$load['cr_asal']=$cr_asal;
		}
		
		if(isset($_GET['cr_driv']) AND $_GET['cr_driv']){
			$cr_driv = $_GET['cr_driv'];
			if($cr_driv)
				$like[] = array('tbl_jenazah_layanan.driver', $cr_driv);
				$load['cr_driv']=$cr_driv;
		}
		      
		 
		if(isset($_GET['ke']) AND $_GET['ke'] AND isset($_GET['dari']) AND $_GET['dari']){
			$to= date('Y-m-d', strtotime($_GET['ke']));
			$from= date('Y-m-d', strtotime($_GET['dari']));
			//$where[] = array('date(`created_at`) BETWEEN \''. $from .'\' AND \''. $to .'\'');
		} else{
			$to= '';
			$from= '';
		}
		
		
        $config['per_page'] = '25';
        
        $preloaddata = array(
            'join' => array(
				array('tbl_jenazah_mobil',  'tbl_jenazah_layanan.mobil = tbl_jenazah_mobil.jenazah_mobil_id', 'left'),
                ),
            'order' => 'id desc',
            'limit' => $config['per_page'],
            'like' => $like,
            'where' => $where,
			'offset' => $offset,
			'whbefr' => $from,
			'whbeto' => $to,
			
        );
        
        $load['datas'] = $this->mm->get('tbl_jenazah_layanan', $preloaddata);
        
		$load['no'] = ($offset) ? $offset + 1 : 1;
        
		$result   = $this->mm->get('tbl_jenazah_layanan', $preloaddata);
		
		
		$jml_data = $this->mm->count('tbl_jenazah_layanan', $preloaddata);
		$load['jml_data'] = $this->mm->count('tbl_jenazah_layanan', $preloaddata);
		$jml_a    = ($offset) ? $offset+1 : 1;
		$jml_b    = (($offset+count($result))!=$jml_data) ? $offset+$config['per_page'] : $jml_data;

		$hal['url_halaman'] = $this->_ctrl.'/index';
		$hal['jml_data']    = $jml_data;
		$hal['jml_a']       = $jml_a;
		$hal['jml_b']       = $jml_b;

		$arr['nomor_hal']   = $jml_a;
		$arr['kunci']       = $this->_kunci;
		$load['_paging'] = $this->hx_tabel->set_halaman($hal,$config['per_page']);
		
		
		$load['driver'] = $this->mm->get('tbl_jenazah_driver');
		$load['mobil'] = $this->mm->get('tbl_jenazah_mobil');
				
        $this->theme->header();
        $this->theme->menu();
        $this->load->view('v_beranda', $load);
        $this->theme->footer();
    }
    
    function tambah()
    {
        $load['title']      = 'Tambah Halaman Pelaporan';
        $load['lebarkolom'] = 'col-md-12';
        $load['aksi']       = 'tambahData';
        $load['url_submit'] = 'simpan';
        
        $breadcrumb = array(
            array(
                'name' => 'Home',
                'icon' => 'icon-home',
                'url' => ''
            ),
            array(
                'name' => 'Beranda',
                'icon' => 'fa fa-alarm'
            )
            
        );
        
        $load['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
        $load['hubungan']   = $this->mm->get('m_hubungan_dgn_almarhum');
        $load['ref_kecamatan']   = $this->mm->get('kecamatan');
		
		$url="https://jss.jogjakota.go.id/siwarga/api/master/prov";
		$datas = file_get_contents($url);
		 
		$load['prov'] = json_decode($datas, true);
        
        $this->theme->header();
        $this->theme->menu();
        $this->load->view('v_beranda_input', $load);
        $this->theme->footer();
    }
    
    function simpan($id = '')
    {
        $post   = array();
        $post2  = array();
        $action = trim(strip_tags($_POST['action']));
        
       
        // foreach ($_POST['data'] as $key => $val) {
			// echo "<pre>";
			// print_r($key);
            
        // }exit;
		
        foreach ($_POST['data'] as $key => $val) {
            $post[$key]         = $val;
            $post['created_at'] = date("Y-m-d H:i:s");
            $post['status'] ='Baru';
        }
        
        if ($action == 'tambahData') {
            $ceking = $this->mm->save('tbl_jenazah_layanan', $post);
            if ($ceking) {
                
                // upload foto
                $user      = 'img/mobil';
                $file_path = "./assets/" . $user . '/';
                
                if (isset($_FILES['foto'])) {
                    
                    $preloaddata = array(
                        'join' => array(),
                        'order' => 'id desc',
                        'limit' => 1
                    );
                    $last_id     = $this->mm->get('tbl_jenazah_layanan', $preloaddata, 'roar');
                    
                    $post2['id_pelayanan'] = $last_id['id'];
                    
                    $files = $_FILES;
                    $cpt   = count($_FILES['foto']['name']);
                    
                    for ($i = 0; $i < $cpt; $i++) {
                        
                        $name = time() . $files['foto']['name'][$i];
                        
                        $_FILES['foto']['name']     = $name;
                        $_FILES['foto']['type']     = $files['foto']['type'][$i];
                        $_FILES['foto']['tmp_name'] = $files['foto']['tmp_name'][$i];
                        $_FILES['foto']['error']    = $files['foto']['error'][$i];
                        $_FILES['foto']['size']     = $files['foto']['size'][$i];
                        
                        $this->upload->initialize($this->set_upload_options($file_path));
                        if (!($this->upload->do_upload('foto')) || $files['foto']['error'][$i] != 0) {
                            print_r($this->upload->display_errors());
                        } else {
                            $uploadData    = $this->upload->data();
                            $filename      = $uploadData['file_name'];
                            $post2['foto'] = $file_path . '' . $filename;
                            //$this->load->model('uploadModel','um');
                            $this->mm->save('tbl_jenazah_foto', $post2);
                        }
                    }
                } else {
                    $this->tambah;
                }
                // upload foto
                $pesan = pesan_warning('success', lang(8, false));
                $this->session->set_flashdata('pesan_warning', $pesan);
            }
        }
        
        if ($action == 'editData') {
            if ($id) {
                $cekricek = $this->mm->get('tbl_jenazah_layanan', array(
                    'where' => array(
                        'id' => $id
                    )
                ), 'roar');
                if (!$cekricek) {
                    $pesan = pesan_warning('success', lang(17, false));
                    $this->session->set_flashdata('pesan_warning', $pesan);
                } elseif ($cekricek) {
                    $the_result = $this->mm->save('tbl_jenazah_layanan', $post, array(
                        'where' => array(
                            'id' => $id
                        )
                    ));
                    if ($the_result) {
                        $pesan = pesan_warning('success', lang(10, false));
                        $this->session->set_flashdata('pesan_warning', $pesan);
                    }
                }
            }
        }
        redirect('beranda');
    }
    
    function edit($id)
    {
        $load['title'] = 'Tambah Halaman Pelaporan';
        
        $load['url_submit'] = 'simpan';
        $load['aksi']       = 'editData';
        
        $breadcrumb = array(
            array(
                'name' => 'Home',
                'icon' => 'icon-home',
                'url' => ''
            ),
            array(
                'name' => 'Beranda',
                'icon' => 'fa fa-alarm'
            )
            
        );
        
        $load['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
		$load['ref_kecamatan']   = $this->mm->get('kecamatan');
        
        $load['result']   = $this->mm->get('tbl_jenazah_layanan', array(
            'where' => array(
                'id' => $id
            )
        ), 'roar');
        $load['hubungan'] = $this->mm->get('m_hubungan_dgn_almarhum');
        
        $this->theme->header();
        $this->theme->menu();
        $this->load->view('v_beranda_input', $load);
        $this->theme->footer();
    }
    
    function hapus($id)
    {
        $hapus   = $this->mm->delete('tbl_jenazah_layanan', array(
            'where' => array(
                'id' => $id
            )
        ));
        $getfoto = $this->mm->get('tbl_jenazah_foto', array(
            'where' => array(
                'id_pelayanan' => $id
            )
        ));
        
        foreach ($getfoto as $gt) {
            $path1 = $gt['foto'];
            unlink($path1); //menghapus gambar di folder produk
        }
        
        $hapus2 = $this->mm->delete('tbl_jenazah_foto', array(
            'where' => array(
                'id_pelayanan' => $id
            )
        ));
        
        if ($hapus2) {
            $pesan = pesan_warning('success', lang(15, false));
            $this->session->set_flashdata('pesan_warning', $pesan);
        }
        redirect(base_url('beranda/beranda'));
    }
    
    function mobil()
    {
        $load['judul']      = 'tambah mobil';
        $load['lebarkolom'] = 'col-md-12';
        $load['aksi']       = 'tambahData';
        
        $this->theme->header();
        $this->load->view('form_mobil', $load);
        $this->theme->footer();
    }
    
    
    
    public function set_upload_options($file_path)
    {
        // upload an image options
        $config                  = array();
        $config['upload_path']   = $file_path;
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }
    
    public function cari()
    {
        $id  = $this->input->post('id');
        //$id  = '3471142704820001';
        $url = 'https://webservice.jogjakota.go.id/ws_nik/index.php/biodata';
        
        $postdata = http_build_query(array(
            'nik' => $id,
            'instansi' => "kec"
        ));
        
        $options = array(
            'http' => array(
                'method' => "POST",
                'content' => $postdata,
                'header' => 'Content-type: application/x-www-form-urlencoded'
            )
        );
        
        $context = stream_context_create($options);
        $file    = file_get_contents($url, false, $context);
        //$data         = json_decode($file, TRUE);
        echo json_encode($file);
    }
    
    function detail($id)
    {
        $load['title'] = 'Detail Pelaporan';
        
        $breadcrumb = array(
            array(
                'name' => 'Home',
                'icon' => 'icon-home',
                'url' => ''
            ),
            array(
                'name' => 'Detail',
                'icon' => 'fa fa-alarm'
            )
            
        );
        
        $load['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
        
		/*
        $load['data']     = $this->mm->get('tbl_jenazah_layanan', array(
            'where' => array(
                'id' => $id
            )
        ), 'roar');
		*/
		
		$where= array();
		$where[] = array('tbl_jenazah_layanan.id',$id);
		$preloaddata = array(
            'join' => array(
            array('tbl_jenazah_mobil', 'tbl_jenazah_layanan.mobil = tbl_jenazah_mobil.jenazah_mobil_id', 'left'),
                ),
            'where' => $where
        );
        
        $load['data'] = $this->mm->get('tbl_jenazah_layanan', $preloaddata,'roar');
		
		
        $load['hubungan'] = $this->mm->get('m_hubungan_dgn_almarhum');
		$load['foto']     = $this->mm->get('tbl_jenazah_foto', array('where' => array('id_pelayanan' => $id)));
        
        $this->theme->header();
        $this->theme->menu();
        $this->load->view('v_detail', $load);
        $this->theme->footer();
    }
    
    function http_request($url)
    {
        // persiapkan curl
        $ch = curl_init();
        
        // set url 
        curl_setopt($ch, CURLOPT_URL, $url);
        
        // set user agent    
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        
        // return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        // $output contains the output string 
        $output = curl_exec($ch);
        
        // tutup curl 
        curl_close($ch);
        
        // mengembalikan hasil curl
        return $output;
    }
    
    
    function cobaservis()
    {
		
        $profile = $this->http_request("https://webservice.jogjakota.go.id/Simpeg/pegawai?nip=" . $this->input->post('id'));
		
		echo json_encode($profile);
		
    }
	
	 function update_status($id)
    {
		
	$data  = $this->mm->get('tbl_jenazah_layanan', array('where' => array('id' => $id)),'roar');

	//print_r($data);exit;
       echo json_encode($data);
    }
    
    public function pelaporan_status()
    {
		 $post   = array();
        if (!empty($this->input->post('old_id')) && !empty($this->input->post('new_status'))) {

           $id      = $this->input->post('old_id');
           $post['status']  =  $this->input->post('new_status');

		   $result = $this->mm->save('tbl_jenazah_layanan', $post, array('where' => array('id' => $id)));

           if ($result) {
                        $pesan = pesan_warning('success', lang(10, false));
                        $this->session->set_flashdata('pesan_warning', $pesan);
                    }
        }
        else
        {
            $this->session->set_flashdata('error', 'Silahkan lengkapi form perubahan status !');
        }
    }
	
	public function add_driver()
    {
		 $post   = array();
        if (!empty($this->input->post('old_id')) && !empty($this->input->post('new_driver'))) {

           $id      = $this->input->post('old_id');
           $post['driver']  =  $this->input->post('new_driver');
           $post['mobil']  =  $this->input->post('new_mobil');

		   $result = $this->mm->save('tbl_jenazah_layanan', $post, array('where' => array('id' => $id)));

           if ($result) {
                $pesan = pesan_warning('success', lang(10, false));
                $this->session->set_flashdata('pesan_warning', $pesan);
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Silahkan lengkapi form perubahan status !');
        }
    }
	
	public function ref_kelurahan($id)
	{
		//$kecamatan = $this->input->post("kecamatan");
		
		$kueri	= $this->mm->get("kelurahan", array('where' => array('id_kecamatan' => $id)));
		
		foreach ($kueri as $value) {
            $data .= "<option value='" . $value['id'] . "'>" . $value['kelurahan'] . "</option>";
        }
        echo $data;
	}
	
	function get_province(){
		$url="https://jss.jogjakota.go.id/siwarga/api/master/prov";
		$datas = file_get_contents($url);
		 
		$data = json_decode($datas, true);
		
		print_r($data);
	}
	
	function get_kabupaten($id_prop){
		//$id_prop = 34;
		$url =  "https://jss.jogjakota.go.id/siwarga/api/master/kab?provId=".$id_prop;
		
		$datas = file_get_contents($url);
		 
		$data = json_decode($datas, true);
		
		foreach ($data as $value) {
            $data .= "<option value='" . $value['id'] . "'>" . $value['name'] . "</option>";
        }
        echo $data;
	}
	
	function get_kecamatan($id_kab){
		//$id_kab = 3471;
		$url =  "https://jss.jogjakota.go.id/siwarga/api/master/kec?kabId=".$id_kab;
		
		$datas = file_get_contents($url);
		 
		$data = json_decode($datas, true);
		
		foreach ($data as $value) {
            $data .= "<option value='" . $value['id'] . "'>" . $value['name'] . "</option>";
        }
        echo $data;
	}
	
	function get_kelurahan($id_kec){
		//$id_kec = 347101;
		$url =  "https://jss.jogjakota.go.id/siwarga/api/master/kel?kecId=".$id_kec;
		
		$datas = file_get_contents($url);
		 
		$data = json_decode($datas, true);
			
		foreach ($data as $value) {
            $data .= "<option value='" . $value['id'] . "'>" . $value['name'] . "</option>";
        }
        echo $data;
	}
    
}

?>