<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?=$app_info->NAME?></title>
		<link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
		<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bs-improve.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/css/footer_2.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/plugins/fontawesome-5.2.0/css/all.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/select2/select2.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/plugins/datatable-1.10.18/datatables.min.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/css/app.css">

		<script src="<?=base_url();?>assets/js/jquery-3.3.1.js" type="text/javascript" ></script>
		<script src="<?=base_url();?>assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/plugins/datatable-1.10.18/datatables.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="<?=base_url();?>assets/plugins/select2/select2.min.js"></script>
    	<script src="<?=base_url();?>assets/plugins/loaders/pace.min.js"></script>
    	<script src="<?=base_url();?>assets/plugins/notifications/bootbox.min.js"></script>
        <script src="<?=base_url();?>assets/js/app.js"></script>
	</head>
	<body>