<nav class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="bar-sosmed">
        <div class="container">
            <a class="navbar-brand" href="#"><b><?=$app_info->NAME?></b></a>
            <div class="pull-right small-icon">
                <a class="text-white" href="#" role="button">Pemerintah Kota Yogyakarta</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbs" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbs">
            <ul class="nav navbar-nav">
                <li id="pengaturan" class="pull-right">
                    <a href="https://jss.jogjakota.go.id/beranda/main"> &nbsp;Dashboard JSS</a>
                </li>
                <?php foreach ($list_menu as $key => $value): ?> 
                    <li class="dropdown" id="menu<?=$value->MENU_ID?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        </a>
                        <?php if (!empty($value->child)) { ?>
                            <li class="dropdown <?=(!empty($value->expand)) ? 'active': null;?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <span class="<?=$value->MENU_ICON?>"></span>  &nbsp;<?=$value->MENU_NAME?> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <?php foreach ($value->child as $key2 => $value2): ?> 
                                        <li class="<?=(!empty($value2->active)) ? 'active': null;?>">
                                            <a href="<?=$value2->MODULE_NAME?>">
                                                <span class="<?=$value2->MENU_ICON?>"></span> &nbsp;<?=$value2->MENU_NAME?>
                                            </a>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </li>
                        <?php } else { ?>
                            <li class="<?=(!empty($value->active)) ? 'active': null;?>">
                                <a href="<?=$value->MODULE_NAME?>">
                                    <span class="<?=$value->MENU_ICON?>"></span>  &nbsp;<?=$value->MENU_NAME?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </li> 
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</nav>
&nbsp;