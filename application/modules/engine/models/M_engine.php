<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_engine extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getMenuParent($role_id) 
	{
		$this->db->select('
			b.MENU_ID,
			b.MENU_NAME,
			b.MENU_ICON,
			b.MENU_PARENT_ID,
			b.MENU_MODULE_ID,
			c.MODULE_NAME
		');
		$this->db->from('ENGINE_MENUACCESS a');
        $this->db->join('ENGINE_MENU b', 'b.MENU_ID = a.MENUACCESS_MENU_ID', 'left');
        $this->db->join('ENGINE_MODULE c', 'c.MODULE_ID = b.MENU_MODULE_ID', 'left');
		$this->db->where('a.MENUACCESS_ROLE_ID', $role_id);
		$this->db->where('b.IS_ACTIVE', '1');
		$this->db->where('b.MENU_PARENT_ID', NULL);
		$this->db->order_by('b.MENU_ORDER', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function getMenuChild($role_id, $menu_parent_id) 
	{
		$this->db->select('
			b.MENU_ID,
			b.MENU_NAME,
			b.MENU_ICON,
			b.MENU_PARENT_ID,
			b.MENU_MODULE_ID,
			c.MODULE_NAME
		');
		$this->db->from('ENGINE_MENUACCESS a');
        $this->db->join('ENGINE_MENU b', 'b.MENU_ID = a.MENUACCESS_MENU_ID', 'left');
        $this->db->join('ENGINE_MODULE c', 'c.MODULE_ID = b.MENU_MODULE_ID', 'left');
		$this->db->where('a.MENUACCESS_ROLE_ID', $role_id);
		$this->db->where('b.MENU_PARENT_ID', $menu_parent_id);
		$this->db->where('b.IS_ACTIVE', '1');
		$this->db->order_by('b.MENU_ORDER', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function getModuleAccess($role_id, $module_name) 
	{
        $this->db->select('
            c.`MODULE_NAME`
        ');
        $this->db->from('ENGINE_MENUACCESS a');
        $this->db->join('ENGINE_MENU b', 'b.`MENU_ID` = a.`MENUACCESS_MENU_ID`');
        $this->db->join('ENGINE_MODULE c', 'c.`MODULE_ID` = b.`MENU_MODULE_ID`');
        $this->db->where('a.MENUACCESS_ROLE_ID', $role_id);
        $this->db->where('c.MODULE_NAME', $module_name);
        $get_access = $this->db->get();
        return $get_access->result_array();
    }

    public function getDataAll($table, $order_column, $order_type)
	{
	    $this->db->order_by("$order_column", "$order_type");
	    $query = $this->db->get("$table");
	    $result = $query->result();
	    $this->db->save_queries = false;

	    return $result;
	}

	public function getDataAllWhere($table, $where, $order_column, $order_type)
	{
	    foreach ($where as $key => $value) {
	    	$this->db->where($key, $value);
	    }
	    $this->db->order_by("$order_column", "$order_type");
	    $query = $this->db->get("$table");
	    $result = $query->result();
	    $this->db->save_queries = false;

	    return $result;
	}

	public function getDataRows($table)
	{
	    $query = $this->db->get("$table");
	    $result = $query->row();
	    $this->db->save_queries = false;

	    return $result;
	}

	public function getDataRowsWhere($table, $where)
	{
	    foreach ($where as $key => $value) {
	    	$this->db->where($key, $value);
	    } 
		if ($value) {
		    $query = $this->db->get("$table");
		    $result = $query->row();
		    $this->db->save_queries = false;
		    return $result;
		} else {
			return false;
		}
	}

	public function getDataDropdown($table, $id, $name)
	{
		$this->db->select("
			$id as `id`,
			$name as `text`
		");
		$this->db->from("$table");
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	public function getDataDropdownWhere($table, $id, $name, $where)
	{
		$this->db->select("
			$id as `id`,
			$name as `text`
		");
		$this->db->from("$table");
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	public function insertData($table, $data)
	{
	    return $this->db->insert("$table", $data);
	}

	public function insertDataReturnLastId($table, $data)
	{
	    $this->db->insert("$table", $data);

	    return $this->db->insert_id();
	}

	public function updateData($table, $data, $id)
	{
	    $this->db->where($id['name'], $id['id']);
	    $this->db->update("$table", $data);

	    if ($this->db->affected_rows()) {
	    	return true;
	    } else {
	    	return false;
		}
	}

	public function updateDataAnyWhere($table, $data, $where)
	{
		foreach ($where as $key => $value) {
			$this->db->where($key, $value);
		}
	    $this->db->update("$table", $data);

	    if ($this->db->affected_rows()) {
	    	return true;
	    } else {
	    	return false;
		}
	}

	public function deleteDataWhere($table, $id)
	{
	    $this->db->where($id['name'], $id['id']);
	    return $this->db->delete("$table");
	}
}