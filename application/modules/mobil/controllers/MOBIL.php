<?php
class MOBIL extends MX_Controller {

    function __construct() {
        parent::__construct();
       $this->load->model(array('mm'));
	   $this->load->module('theme');
	   $this->load->library(array('pagination','upload','layouts'));
    }
	
	var $redirectpage = 'MOBIL';
    var $tablename = 'tbl_jenazah_mobil';
	
	function index($offset = null){
		$load['title']='DRIVER';
		$load['lebarkolom']='col-md-12';
		$load['url_simpan'] = $this->redirectpage . '/simpan';
        $load['url_hapus'] = $this->redirectpage . '/hapus';
		
		$breadcrumb    = array(
            array(
                'name' => 'Home',
                'icon' => 'icon-home',
                'url' => ''
            ),
            array(
                'name' => 'DRIVER',
                'icon' => 'fa fa-alarm'
            )
        );
        
        $load['breadcrumb'] = $this->layouts->make_breadcrumb($breadcrumb);
		
		
		$preloaddata = array(
            'join' => array(
            //array('ref_spesifikasi', $this->tablename .'.spek_id = ref_spesifikasi.spek_id', 'left'),
            ),
            //'order' => $this->tablename . '.klasifikasi_id asc',
            //'limit' => $config['per_page'],
            'offset' => $offset
        );
        $load['datas'] = $this->mm->get($this->tablename, $preloaddata);
        $load['no'] = ($offset) ? $offset + 1 : 1;

        $load['prime_key'] = 'jenazah_mobil_id';
        $load['second_key'] = 'jenazah_mobil_plat';
        $load['arr_field'] = array(
            'jenazah_mobil_id' => array(
            ),
			'jenazah_mobil_plat' => array(
                'table' => 1,
                'label' => 'Plat Nomor',
                'form' => 1,
                'form_label' => 'Plat Nomor',
                'form_type' => 'text',
            ),
			
			'jenazah_mobil_merk' => array(
                'table' => 1,
                'label' => 'Merk Mobil',
                'form' => 1,
                'form_label' => 'Merk Mobil',
                'form_type' => 'text',
            ), 
			
			'jenazah_mobil_tahun' => array(
                'table' => 1,
                'label' => 'Tahun Mobil',
                'form' => 1,
                'form_label' => 'Tahun Mobil',
                'form_type' => 'text',
            ), 	
			
        );
		
        $this->theme->header();
        $this->theme->menu();
        $this->load->view('v_mobil', $load);
        $this->theme->footer();
	}

	function simpan() {
        $post = array();
        $action = trim(strip_tags($_POST['action']));

        foreach ($_POST['data'] as $key => $val) {
            $post[$key] = ucwords(trim(strip_tags($val)));
        }
        if ($action == 'tambahData') {
            $ceking = $this->mm->save($this->tablename, $post);
            if ($ceking) {
                $pesan = pesan_warning('success', lang(8, false));
                $this->session->set_flashdata('pesan_warning', $pesan);
            }
        }
        if ($action == 'editData') {
            $dataID = isset($_POST['dataID']) ? trim(strip_tags($_POST['dataID'])) : '';
            $prime_key = isset($_POST['prime_key']) ? trim(strip_tags($_POST['prime_key'])) : '';
            if ($dataID) {
                $cekricek = $this->mm->get($this->tablename, array('where' => array($prime_key => $dataID)), 'roar');
                if (!$cekricek) {
                    $pesan = pesan_warning('success', lang(17, false));
                    $this->session->set_flashdata('pesan_warning', $pesan);
                } elseif ($cekricek) {
                    $the_result = $this->mm->save($this->tablename, $post, array('where' => array($prime_key => $dataID)));
                    if ($the_result) {
                        $pesan = pesan_warning('success', lang(10, false));
                        $this->session->set_flashdata('pesan_warning', $pesan);
                    }
                }
            }
        }
        redirect(site_url($this->redirectpage));
    }

    function hapus() {
        $action = trim(strip_tags($_POST['action']));

        if ($action == 'hapusData') {
            $dataID = isset($_POST['dataID']) ? trim(strip_tags($_POST['dataID'])) : '';
            $prime_key = isset($_POST['prime_key']) ? trim(strip_tags($_POST['prime_key'])) : '';
            if ($dataID) {
                $cekricek = $this->mm->get($this->tablename, array('where' => array($prime_key => $dataID)), 'roar');
                if (!$cekricek) {
                    $pesan = pesan_warning('success', lang(16, false));
                    $this->session->set_flashdata('pesan_warning', $pesan);
                } elseif ($cekricek) {
                    $the_result = $this->mm->delete($this->tablename, array('where' => array($prime_key => $dataID)));
                    if ($the_result) {
                        $pesan = pesan_warning('success', lang(15, false));
                        $this->session->set_flashdata('pesan_warning', $pesan);
                    }
                }
            }
        }
        redirect(site_url($this->redirectpage));
    }
	
}
?>