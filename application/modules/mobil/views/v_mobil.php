<script type="text/javascript">
$(document).ready(function () {
/*    $('.date').datetimepicker({
      format: 'YYYY-MM-DD',
      locale: 'id'
    });
    $('.viewjam').datetimepicker({
      format: 'HH:mm:ss',
      locale: 'id'
    });
   $('.chosen-select').chosen({placeholder_text_single: '-',no_results_text: 'Tidak Ditemukan'});
   $(".select2").select2();*/
});
	function tambahDataParent(parentID){
	   $('#tambahFormBox').modal('show');
      $('#theFormData #parentKategori').val(parentID);
	}
	function hapusKonf( ids, hapusnama ){
	   $('#hapusKonf').modal('show');
		$('#hapusDataForm #hapusDataID').val(ids);
      $('#hapusNamaData').html(hapusnama);
      <?php if(isset($jqueryhapus) AND $jqueryhapus){ echo $jqueryhapus; } ?>
	}
	function editDataForm( ids, thevaluedata ){
	   $('#editFormBox').modal('show');
		$('#editFormData #editDataID').val(ids);
      <?php if(isset($jqueryedit) AND $jqueryedit){ echo $jqueryedit; } ?>
      <?php $hitungform = 1; foreach($arr_field as $kfield => $vfield){ if(isset($vfield['form']) AND $vfield['form']){ ?>
      <?php $hitungform++; ?>
         $('#editFormData #edit<?php echo $kfield; ?>').val(thevaluedata.<?php echo $kfield; ?>);
         <?php if($vfield['form_type'] == 'select2'){ ?>
            $('#edit<?php echo $kfield; ?>').chosen().val(thevaluedata.<?php echo $kfield; ?>);
            $('#edit<?php echo $kfield; ?>').trigger("chosen:updated");
         <?php } ?>
      <?php } } ?>
	}
	function editDataForm2( ids, <?php if(is_array($arr_field)) echo implode(', ', array_keys($arr_field)); ?> ){
	   $('#editFormBox').modal('show');
		$('#editFormData #editDataID').val(ids);
      <?php foreach($arr_field as $kfield => $vfield){ ?>
		   $('#editFormData #edit<?php echo $kfield; ?>').val(<?php echo $kfield; ?>);
      <?php } ?>
      //alert(korban_tgl_lahir);
	}
<?php if(isset($jquery) AND $jquery){ echo $jquery; } ?>
</script>

<section class="content">
  <div class="container">
    <?=$breadcrumb?>
    <div class="space">
     <?php // print_r($this->session->all_userdata());exit; ?>
     <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="box-content">
          <div class="title-case-line">
           <p><?=$title?></p>
         </div>
         
        
				<table class="table table-bordered table-hover" >
    <thead>
        <tr>
            <th class="text-center" style="width: 1%;">No</th>
            <?php foreach($arr_field as $kfield => $vfield){ if(isset($vfield['table']) AND $vfield['table']){ ?>
               <th><?php echo $vfield['label']; ?></th>
            <?php } } ?>
            <th style="width: 1%;" colspan="2" class="text-center"><button class="btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#tambahFormBox"><span class="glyphicon glyphicon-plus"></span> Tambah</button></th>
        </tr>
    </thead>
        <?php if(isset($datas) AND $datas) {  ?>
    <tbody>
        <?php $num = 1; foreach($datas as $vdatas){ $numzz = $num++; $arr_fieldval = array(); ?>
            <tr>
                <td class="text-center" onclick="editDataForm('<?php //echo $parentDataK; ?>','<?php //echo $parentDataV; ?>', '')"><?php echo $no; ?>.</td>
               <?php $obj = '{'; ?>
               <?php foreach($arr_field as $kfield => $vfield){ if(isset($vfield['table']) AND $vfield['table']){ ?>
                  <td><?php echo $vdatas[$kfield]; ?></td>
               <?php } ?>
               <?php //$arr_fieldval[] = $vdatas[$kfield]; ?>
               <?php $obj .= "'". $kfield ."':'". $vdatas[$kfield] ."',"; ?>
               <?php } ?>
               <?php $obj .= '0:0}'; ?>
                <td class="text-center pointer" onclick="editDataForm('<?php echo $vdatas[$prime_key]; ?>',<?php //if(is_array($arr_fieldval)) echo implode("', '", $arr_fieldval); ?><?php echo $obj; ?>);"><span class="glyphicon warnaijo glyphicon-pencil"></span></td>
                <td class="text-center pointer" onclick="hapusKonf('<?php echo $vdatas[$prime_key]; ?>','<?php echo $vdatas[$second_key]; ?>')"><span class="glyphicon warnamerah glyphicon-remove"></span></td>
            </tr>
        <?php $no++;} } ?>

    </tbody>
    </table>
		
		
		
         </div>
       </div>
     </div>
   </div>
 </div>
</section>


 <?php
      if(isset($lebarlabelform) AND $lebarlabelform) {$lebarlabelform = $lebarlabelform;}else{$lebarlabelform = 25;}
      $cutform = ceil($hitungform / 2);
      $collg = 12;
   ?>
	<div id="tambahFormBox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog <?php if($hitungform >= 9) {echo 'modal-lg';$collg=6; } ?>">
		  <div class="modal-content">
		<form method="post" action="<?php echo site_url($url_simpan); ?>" id="theFormData">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			  <h4 class="modal-title"><span class="glyphicon glyphicon-plus"></span> Tambah <?php echo $title; ?></h4>
			</div>
			<div class="modal-body bg-white <?php //if($hitungform > $cutform) {echo 'kolom2'; } ?>">
					  <input type="hidden" name="action" value="tambahData" />
                  <div class="row">
                  <div class="col-lg-<?php echo $collg; ?>">
                  <table class="table table-borderless">
                        <?php $az = 1; foreach($arr_field as $kfield => $vfield){ if(isset($vfield['form']) AND $vfield['form']){ $az++; ?>
                           <tr>
                           	<td width="<?php echo $lebarlabelform; ?>%" align="right"><label for="<?php echo $kfield; ?>"><?php echo (isset($vfield['form_label']) AND $vfield['form_label']) ? $vfield['form_label'] : $vfield['label']; ?></label></td>
                           	<td width="1%">:</td>
                           	<td>
                                 <?php switch($vfield['form_type']){ case "text": ?>
                                    <input type="text" name="data[<?php echo $kfield; ?>]" id="<?php echo $kfield; ?>" value="" class="form-control input-s-m" />
                                 <?php break; case "area": ?>
                                    <textarea name="data[<?php echo $kfield; ?>]" id="<?php echo $kfield; ?>" value="" class="form-control input-s-m"></textarea>
                                 <?php break; case "number": ?>
                                    <input type="number" name="data[<?php echo $kfield; ?>]" id="<?php echo $kfield; ?>" min="0" value="" class="form-control input-s-m" />
                                 <?php break; case "date": ?>
                                    <input type="text" name="data[<?php echo $kfield; ?>]" id="<?php echo $kfield; ?>" value="" class="form-control input-s-m viewdate" />
                                 <?php break; case "select2": ?>
                                    <select name="data[<?php echo $kfield; ?>]" id="<?php echo $kfield; ?>" class="chosen-select form-control input-s-m">
                                       <option value=""></option>
                                       <?php foreach($vfield['keyvaldata'] as $kcombo => $vcombo){ ?>
                                          <option value="<?php echo $kcombo; ?>" data-class="fa fa-square-o"><?php echo $vcombo; ?></option>
                                       <?php } ?>
                                    </select>
                                 <?php break; case "select": ?>
                                    <select name="data[<?php echo $kfield; ?>]" id="<?php echo $kfield; ?>" class="form-control input-s-m">
                                       <option value=""></option>
                                       <?php foreach($vfield['keyvaldata'] as $kcombo => $vcombo){ ?>
                                          <option value="<?php echo $kcombo; ?>" data-class="fa fa-square-o"><?php echo $vcombo; ?></option>
                                       <?php } ?>
                                    </select>
                                 <?php break; }  ?>
                              </td>
                           </tr>
                        <?php if($az >= $cutform) {echo '</table></div><div class="col-lg-'. $collg .'"><table class="table table-borderless">'; } ?>
                        <?php } } ?>
                    </table>
                    </div>
                    </div>
			</div>
			<div class="modal-footer">
                    <div class="pull-left">
                        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-arrow-left"></span> Batal</button>
                    </div>
                <button class="btn btn-primary btn-sm" type="submit"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
			</div>
		</form>
		  </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	
	<div id="editFormBox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog <?php if($hitungform >= 9) {echo 'modal-lg';$collg=6; } ?>">
		  <div class="modal-content">
			<form method="post" action="<?php echo site_url($url_simpan); ?>" id="editFormData">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Ubah <?php echo $title; ?></h4>
				</div>
				<div class="modal-body bg-white">
					  <input type="hidden" name="action" value="editData" />
                 <input type="hidden" name="prime_key" value="<?php echo $prime_key; ?>" />
					  <input type="hidden" name="dataID" id="editDataID" value="">
                  <div class="row">
                  <div class="col-lg-<?php echo $collg; ?>">
                    <table class="table table-borderless">
                        <?php $az = 1; foreach($arr_field as $kfield => $vfield){ if(isset($vfield['form']) AND $vfield['form']){ $az++; ?>
                           <tr>
                           	<td width="25%" align="right"><label for="edit<?php echo $kfield; ?>"><?php echo (isset($vfield['form_label']) AND $vfield['form_label']) ? $vfield['form_label'] : $vfield['label']; ?></label></td>
                           	<td width="1%">:</td>
                           	<td>
                                 <?php switch($vfield['form_type']){ case "text": ?>
                                    <input type="text" name="data[<?php echo $kfield; ?>]" id="edit<?php echo $kfield; ?>" value="" class="form-control input-s-m" />
                                 <?php break; case "area": ?>
                                    <textarea name="data[<?php echo $kfield; ?>]" id="edit<?php echo $kfield; ?>" value="" class="form-control input-s-m"></textarea>
                                 <?php break; case "number": ?>
                                    <input type="number" name="data[<?php echo $kfield; ?>]" id="edit<?php echo $kfield; ?>" min="0" value="" class="form-control input-s-m" />
                                 <?php break; case "date": ?>
                                    <input type="text" name="data[<?php echo $kfield; ?>]" id="edit<?php echo $kfield; ?>" value="" class="form-control input-s-m viewdate" />
                                 <?php break; case "select2": ?>
                                    <select name="data[<?php echo $kfield; ?>]" id="edit<?php echo $kfield; ?>" class="chosen-select form-control">
                                       <option value=""></option>
                                       <?php foreach($vfield['keyvaldata'] as $kcombo => $vcombo){ ?>
                                          <option value="<?php echo $kcombo; ?>" data-class="fa fa-square-o"><?php echo $vcombo; ?></option>
                                       <?php } ?>
                                    </select>
                                 <?php break; case "select": ?>
                                    <select name="data[<?php echo $kfield; ?>]" id="edit<?php echo $kfield; ?>" class="form-control input-s-m">
                                       <option value=""></option>
                                       <?php foreach($vfield['keyvaldata'] as $kcombo => $vcombo){ ?>
                                          <option value="<?php echo $kcombo; ?>" data-class="fa fa-square-o"><?php echo $vcombo; ?></option>
                                       <?php } ?>
                                    </select>
                                 <?php break; }  ?>
                              </td>
                           </tr>
                        <?php if($az >= $cutform) {echo '</table></div><div class="col-lg-'. $collg .'"><table class="table table-borderless">'; } ?>
                        <?php } } ?>
                    </table>
               </div>
               </div>
            </div>
				<div class="modal-footer">
                    <div class="pull-left">
                        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-arrow-left"></span> Batal</button>
                    </div>
				    <button class="btn btn-primary btn-sm" type="submit"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
				</div>
			</form>
		  </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	
	<div id="hapusKonf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
		  <div class="modal-content">
			<form method="post" action="<?php echo site_url($url_hapus); ?>" id="hapusDataForm">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h4 class="modal-title">Hapus <?php echo $title; ?></h4>
				</div>
				<div class="modal-body bg-white">
					  <input type="hidden" name="action" value="hapusData">
                 <input type="hidden" name="prime_key" value="<?php echo $prime_key; ?>" />
					  <input type="hidden" name="dataID" id="hapusDataID" value="">
                    Yakin akan menghapus data <strong id="hapusNamaData"></strong>?
                </div>
				<div class="modal-footer">
                    <div class="pull-left">
                        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-arrow-left"></span> Batal</button>
                    </div>
                    <button class="btn btn-danger btn-sm" type="submit"><span class="glyphicon glyphicon-remove"></span> Hapus</button>
				</div>
			</form>
		  </div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	