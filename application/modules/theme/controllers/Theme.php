<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Theme extends MX_Controller {
	
	function __construct() {
        parent::__construct();
		//$this->load->model(array('mm'));
		//$this->load->library(array('pagination','upload'));
		//$this->load->helper(array('url','html','form'));
    }
	
	
    function header()
	{
		$this->load->view('v_header');
	}

	function menu()
	{
		$this->load->view('v_navbar');
	}

	function footer()
	{
		$this->load->view('v_footer');
	}
}

?>