<!--footer>
	<div class="copyright">
	<div class="container">
		<div class="col-md-6">
			<p>© 2019 - All Rights Pemerintah Kota Yogyakarta</p>
		</div>
		<div class="col-md-6">
			<ul class="bottom_ul">
				<li><a href="#">Sistem Informasi Mobil Jenazah</a></li>
			</ul>
		</div>
	</div>
</div>
</footer-->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 footer-col">
				<p>UPIK merupakan fasilitas bagi masyarakat untuk menyampaikan Keluhan, Pertanyaan, Informasi, maupun Usul/Saran guna pengembangan pelayanan Pemerintah Kota Yogyakarta dan pembangunan Kota Yogyakarta..</p>
				<p><i class="fa fa-map-pin"></i> Jl. Kenari 56, Umbulharjo, Yogyakarta</p>
				<p><i class="fa fa-phone"></i> No. Telp : (0274) 561 270</p>
				<p><i class="fa fa-envelope"></i> E-mail : upik@jogjakota.go.id</p>
				
			</div>
			<div class="col-md-3 col-sm-6 footer-col">
				<h6 class="heading7">Link Terkait</h6>
				<ul class="footer-ul">
					<li><a target='_blank' href="https://jogjakota.go.id">Portal Kota Jogja</a></li>
					<li><a target='_blank' href="https://kominfo.jogjakota.go.id/">Dinas Komunikasi Informatika dan Persandian</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 footer-col">
				<h6 class="heading7">Selayang Pandang</h6>
				<div class="post">
					<p>Berlandaskan Segoro Amarto kita mantapkan daya saing dan perekonomian wilayah menuju Kota Yogyakarta yang lebih baik berkarakter berbudaya nyaman aman maju dan sejahtera <span>Segoro Amarto</span></p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 footer-col">
				<h6 class="heading7">Jogja Smart Service</h6>
				<center>
					<img src="https://jss.jogjakota.go.id/upik/frontend/assets/logo/pemkot.png" class="img img-responsive" alt="Image" style="height: 150px;">
				</center>
			</div>
		</div>
	</div>
	<div class="copyright">
	<div class="container">
		<div class="col-md-6">
			<p>Versi. 2.0.2 © 2019 - Hak Cipta oleh Pemerintah Kota Yogyakarta</p>
		</div>
		<div class="col-md-6">
			<p class="pull-right">Unit Pelayanan Informasi dan Keluhan</p>
		</div>
	</div>
</div>
</footer>
<!--footer start from here-->
<style type="text/css" media="screen">
	#footer:before{
		content: ' ';
	    display: block;
	    position: absolute;
	    left: 0;
	    top: 0;
	    width: 100%;
	    height: 100%;
		background-image: url('https://jss.jogjakota.go.id/upik/frontend/assets/ikon/batik.png');
  		background-repeat: repeat;
  		opacity:0.1;
	}


	.navbar:before{
		content: ' ';
	    display: block;
	    position: absolute;
	    left: 0;
	    top: 0;
	    width: 100%;
	    height: 100%;
		background-image: url('https://jss.jogjakota.go.id/upik/frontend/assets/ikon/batik.png');
  		background-repeat: repeat;
  		opacity:0.3;
	}		
</style>


<script src="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js');?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.tanggal').datepicker({
            format: "dd-mm-yyyy",
            autoclose:true
        });
    });
</script>
<script>
	jQuery(document).ready(function() {
    $('.chosen-container').removeAttr('style');
    $('.chosen-container').attr('style', 'width: 100%;');
    /*$('.chosen-select').chosen({placeholder_text_single: '-Pilihan-',no_results_text: 'Tidak Ditemukan'});*/

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "2500",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "slideDown",
		"hideMethod": "slideUp"
		}
	});
	setTimeout(function() {
		$('.alert').removeClass( "slideInUp" ).addClass( "jello" );
		}, 1500);
		setTimeout(function() {
		$('.alert').removeClass( "jello" ).addClass( "slideOutDown" );
	}, 4000);
		</script>
		
		
      <?php if($pesan_warning=sessf('pesan_warning')) { echo $pesan_warning; } ?>