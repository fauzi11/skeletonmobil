<?php
$uri1 = $this->uri->segment(1);
$uri2 = $this->uri->segment(2);
?>

<style>
.navbar:before {
    content: ' ';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url(https://jss.jogjakota.go.id/upik/frontend/assets/ikon/batik.png);
    background-repeat: repeat;
    opacity: 0.3;
}


</style>

<!--nav class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="bar-sosmed">
        <div class="container">
            <a class="navbar-brand" style="padding: 0; height: 0;" href="#"><b><div style="padding-top: 10px;">Mobil Jenasah</div></b></a>
            <div class="pull-right small-icon">
                <a class="text-white" href="#" role="button">Pemerintah Kota Yogyakarta</a>
                
            </div>
        </div>
    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbs" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbs">
            <ul class="nav navbar-nav">
				<li id="pengaturan" class="pull-right">
                    <a class="btn btn-danger" href="<?php echo  base_url('login/login/logout');?>" role="button">logout</a>
                </li>
                <li id="pengaturan" class="pull-right">
                    <a href="https://jss.jogjakota.go.id/beranda/main"> &nbsp;Dashboard</a>
					
                </li>
				
				
				<li class="<?php if($uri1=="beranda"){echo "active";} ?>">
                    <a href="<?php echo base_url('beranda/beranda');?>">
                        <span class="fa fa-home"></span>  &nbsp;Beranda</span>
                    </a>
                </li>
				
				<li class="<?php if($uri1=="user"){echo "active";} ?>">
                    <a href="<?php echo base_url('user');?>">
                        <span class="fa fa-user"></span>  &nbsp;User</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav-->
&nbsp;


<nav class="navbar navbar-default navbar-fixed-top navbar-custom">
   <div class="container" >
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbs" aria-expanded="false">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="navbs" >
         <ul class="nav navbar-nav">
            <li class="<?php if($uri1=="beranda"){echo "active";} ?>">
                    <a href="<?php echo base_url('beranda/beranda');?>">
                        <span class="fa fa-home"></span>  &nbsp;Beranda</span>
                    </a>
                </li>
			
			<li class="<?php if($uri1=="DRIVER"){echo "active";} ?>">
                    <a href="<?php echo base_url('DRIVER');?>">
                        <span class="fa fa-id-badge"></span>  &nbsp;Driver</span>
                    </a>
            </li>
			
			<li class="<?php if($uri1=="MOBIL"){echo "active";} ?>">
                    <a href="<?php echo base_url('MOBIL');?>">
                        <span class="fa fa-ambulance"></span>  &nbsp;Mobil</span>
                    </a>
            </li>
			
				
			<li class="<?php if($uri1=="user"){echo "active";} ?>">
                    <a href="<?php echo base_url('user');?>">
                        <span class="fa fa-user"></span>  &nbsp;User</span>
                    </a>
            </li>
            
            
            <li id="pengaturan" class="pull-right"><a href="https://jss.jogjakota.go.id/beranda/main"> &nbsp;Dashboard JSS</a></li>
			<li id="pengaturan" class="pull-right" style="color: red;"><a href="<?php echo base_url('login/login/logout');?>"> &nbsp;Logout</a></li>
         </ul>
      </div>
      <!-- /.navbar-collapse -->
   </div>
   <!-- /.container-fluid -->
</nav>