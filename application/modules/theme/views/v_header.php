<?php
$sesi = $this->session->userdata('username');
         if (EMPTY($sesi)) {
           redirect('login/Login');
        }
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" type="image/ico" href="https://jss.jogjakota.go.id/asset/img/favicon.ico">

		<title>Mobil Jenazah</title>
		<script src="<?=base_url();?>assets/js/jquery-3.3.1.js" type="text/javascript" ></script>
		
		<link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet'>
		<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bs-improve.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/css/footer_2.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/plugins/fontawesome-5.2.0/css/all.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/datepicker/datepicker3.css">
		<link href="<?=base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/select2/select2.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/plugins/datatable-1.10.18/datatables.min.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/css/app.css">


		<script src="<?=base_url();?>assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url();?>assets/plugins/datatable-1.10.18/datatables.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="<?=base_url();?>assets/plugins/select2/select2.min.js"></script>
    	<script src="<?=base_url();?>assets/plugins/loaders/pace.min.js"></script>
    	<script src="<?=base_url();?>assets/plugins/notifications/bootbox.min.js"></script>
        <script src="<?=base_url();?>assets/js/app.js"></script>
		

       <!--untuk notif -->
      <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/d_style.css" rel="stylesheet" type="text/css" />


	</head>
	<body>