<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model(array('mm'));
		$this->load->library(array('pagination','upload'));
		$this->load->helper(array('url','html','form'));
		$this->load->module('theme');
    }
	
	function index(){
		$this->load->view('login');
	}
	
	function signintojss(){
		$user = $_POST['username'];
        $pass = $_POST['password'];
		
		
		$data = array(
		            "username"       => $user,
		            "password"       => $pass,
			);

	    $url = "https://layananupik.jogjakota.go.id/lumen/public/api/web/login";

	    $options = array(
	        'http' => array(
	            'method'    => 'POST',
	            'content'   => json_encode($data),
	            'header'    =>  "Content-Type: text/plain\r\n".
	                            "Accept: application/json\r\n"
	            )
	    );

	    $context     = stream_context_create($options);
	    $result      = file_get_contents($url, false, $context);
	    $response    = json_decode($result);

	    $hasil = $response->{'data'};
		
		
	    //echo $hasil->{'id_upik'};
		$idjss = $hasil->{'id_upik'};
		$getdata = $this->mm->get('tbl_jenazah_user', array('where' => array(				'user_jenazah_jss' => $idjss)));
		
		
		//print_r(in_array($nikasli, $getdata));exit;
		

		if ($response->{'status'}==true && ($getdata))
		{
			$user = array(
				'username' 	        	=> $hasil->{'username'},
				'nama' 	        		=> $hasil->{'nama'},
				'id_upik' 	        	=> $hasil->{'id_upik'},
				'id_user' 	        	=> $hasil->{'id_user'},
				'foto' 	        		=> $hasil->{'foto'},
				'nik' 	        		=> $hasil->{'nik'},
				'email' 	        	=> $hasil->{'email'},
				'alamat' 	        	=> $hasil->{'alamat'},
				'no_telp' 	        	=> $hasil->{'no_telp'},
				'role' 	        		=> $hasil->{'role'},
				'token' 	        	=> $hasil->{'token'},
				'nama_role' 	    	=> $hasil->{'nama_role'},
				'skpd' 	        		=> $hasil->{'skpd'},
				'nama_skpd' 	        => $hasil->{'nama_skpd'},
				'nip' 	        		=> $hasil->{'nip'},
				//'unit_id' 	        	=> $hasil->{'unit_simpeg'}
			);

			//var_dump($user);
			$this->session->set_userdata($user);
			$pesan = pesan_warning('success', lang(22, false));
			$this->session->set_flashdata('pesan_warning', $pesan);
			
			redirect(base_url('beranda/beranda'));
		}
		else
		{
			//echo "Error";
			$this->session->set_userdata($user);
			$pesan = pesan_warning('warning', lang(69, false));
			$this->session->set_flashdata('pesan_warning', $pesan);
			
			redirect('login');
		}
	}
	
	public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('login/Login'));
    }
}

?>