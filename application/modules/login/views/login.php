<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Login</title>

   
	<link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap-3.3.7/css/bootstrap.min.css">
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/css/d_style.css" rel="stylesheet" type="text/css" />
<style>
		body{
    background: #F8DA56;
}
.form-login{
    margin-top: 13%;
}
.outter-form-login {
    padding: 20px;
    background: #EEEEEE;
    position: relative;
    border-radius: 5px;
}
.logo-login {
    position: absolute;
    font-size: 35px;
    background: #21A957;
    color: #FFFFFF;
    padding: 10px 18px;
    top: -40px;
    border-radius: 50%;
    left: 40%;
}
.inner-login .form-control {
    background: #D3D3D3;
}
h3.title-login {
    font-size: 20px;
    margin-bottom: 20px;
}

.forget {
    margin-top: 20px;
    color: #ADADAD;
}
.btn-custom-green {
    background: #21A957;
    color: #fff;
}
	
</style>
	
  </head>
  <body>
    <div class="col-md-4 col-md-offset-4 form-login">   

        <div class="outter-form-login">
        <div class="logo-login">

            <em class="glyphicon glyphicon-user"></em>
        </div>
            <form action="<?php echo  base_url('login/login/signintojss'); ?>" class="inner-login" method="post">
            <h3 class="text-center title-login">Login Pengguna</h3>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                
                <input type="submit" class="btn btn-block btn-custom-green" value="LOGIN" />
                
                <div class="text-center forget">
                   <a href="https://jss.jogjakota.go.id/lupa_sandi"> <p>Forgot Password ?</p></a>
                </div>
            </form>
        </div>
    </div>

    <script src="<?=base_url();?>assets/js/jquery-3.3.1.js" type="text/javascript" ></script>
    <script src="<?=base_url();?>assets/plugins/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
	
	<script>
	jQuery(document).ready(function() {
    $('.chosen-container').removeAttr('style');
    $('.chosen-container').attr('style', 'width: 100%;');
    /*$('.chosen-select').chosen({placeholder_text_single: '-Pilihan-',no_results_text: 'Tidak Ditemukan'});*/

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-bottom-right",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "2500",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "slideDown",
		"hideMethod": "slideUp"
		}
	});
	setTimeout(function() {
		$('.alert').removeClass( "slideInUp" ).addClass( "jello" );
		}, 1500);
		setTimeout(function() {
		$('.alert').removeClass( "jello" ).addClass( "slideOutDown" );
	}, 4000);
		</script>
		
		
      <?php if($pesan_warning=sessf('pesan_warning')) { echo $pesan_warning; } ?>
	
  </body>
</html>