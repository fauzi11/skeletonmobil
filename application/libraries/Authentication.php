<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication
{
    private $CI;
    private $message = array(
        'notsigned' => 'Anda harus login terlebih dahulu !',
        'invalid'   => 'Anda tidak diizinkan untuk mengakses !',
        'logout'    => 'Anda telah logout !'
    );

    public function __construct() 
    {
        $this->CI            = &get_instance();
        $this->currentModule = $this->CI->router->fetch_class();
        $this->signedRole    = $this->CI->session->userdata('id_role');
        $this->CI->load->model('engine/m_engine');
    }

    public function authorize() 
    {
        $userdata = $this->CI->session->userdata();
        if (!empty($userdata['id_user'])) {
            $this->checkAccess();
        } else {
            $this->checkToken();
            $this->checkAccess();
        }
    }

    public function checkAccess() 
    {
        $access = $this->CI->m_engine->getModuleAccess($this->signedRole, $this->currentModule);
		if (empty($access)) {
            $this->CI->session->set_flashdata('alert', array('danger', 'Error', $this->message['invalid']));
            redirect($_SERVER['HTTP_REFERER']);     
        }
    }

    public function checkToken()
    {
        if(!empty($this->CI->input->get('token'))) {
            $token = str_replace(' ', '+', $this->CI->input->get('token'));
            $url = 'https://webservice.jogjakota.go.id/jss/token?token='.$token;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            $userdata = json_decode($result, true);

            if (!empty($userdata[0])) {
                $this->CI->session->set_userdata($userdata[0]);
                redirect(base_url().$this->currentModule);
            } else {
                redirect('https://jss.jogjakota.go.id');
            }
        } else {
            redirect('https://jss.jogjakota.go.id');
        }
    }

    public function logoutTest()
    {
        // because error flashdata after session destroy
        foreach ($this->CI->session->userdata() as $key => $value) {
            if ($key !='__ci_last_regenerate') {
                $this->CI->session->unset_userdata($key);
            }
        }
        $this->CI->session->set_flashdata('alert', array('danger', 'Error', $this->message['logout']));
    }
}
?>