<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Hashids\Hashids;
class Encoder {

	var $CI;

	function  __construct() {
		$this->CI =& get_instance();
		$this->encryption_key = 'diskominfo';
		$this->hash = new Hashids($this->encryption_key, 7);
	}

	function encrypt($data) {
		return $this->hash->encode($data);
	}

	function decrypt($data) {
		$decoded = $this->hash->decode($data);
		if (count($decoded) == 1) {
			return $decoded[0];
		} else {
			return $decoded;
		}
	}
}