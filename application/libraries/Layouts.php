<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Layouts {

	var $CI;

	function  __construct() {
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->load->model('engine/m_engine');
	}

	function dressing($view, $data = null) {
		$app_info = $this->CI->m_engine->getDataRows('APP_INFO');
		$header['app_info'] = $app_info;
		$navbar['app_info'] = $app_info;
		$navbar['active_menu'] = $this->CI->router->fetch_class();

		$role = $this->CI->session->userdata('id_role');
		$list_menu = $this->CI->m_engine->getMenuParent($role);

		foreach ($list_menu as $key => $value) {
			$child = $this->CI->m_engine->getMenuChild($role, $value->MENU_ID);
			if ($child) {
				$list_menu[$key]->child	= $child; // 
				foreach ($child as $key2 => $value2) {
					$grandChild = $this->CI->m_engine->getMenuChild($role, $value2->MENU_ID);
					if ($grandChild) {
						$list_menu[$key]->child[$key2]->grandChild = $grandChild;
						foreach ($grandChild as $key3 => $value3) {
							if ($value3->MODULE_NAME == $navbar['active_menu']) {
								$value3->active = true;
								$value2->expand = true;
								$value->expand = true;
							}
						}
					} else {
						if ($value2->MODULE_NAME == $navbar['active_menu']) {
							$value2->active = true;
							$value->expand = true;
						}
					}
				}
			} else {
				if ($value->MODULE_NAME == $navbar['active_menu']) {
					$value->active = true;
				}
			}
		}
		// set data menu on navbar or head ??
		$navbar['list_menu'] = $list_menu;	

		$this->CI->load->view('engine/v_header', $header);
		$this->CI->load->view('engine/v_navbar', $navbar);
		$this->CI->load->view($view, $data);
		$this->CI->load->view('engine/v_footer');

		
	}

	function make_breadcrumb($data) {
		$str = '<ol class="breadcrumb">';

		$lastElement = end($data);
		foreach ($data as $key => $val) {
			if ($val !== $lastElement) {
				$str .= '<li><a href="' . base_url() . $val['url'] . '">';
				if (!empty($val['icon'])) {
					$str .= '<i class="' . $val['icon'] . ' position-left"></i> ';
				}
				$str .= $val['name'] . '';
				$str .= '</a></li>';
			} else {
				$str .= '<li class="active">';
				if (!empty($val['icon'])) {
					$str .= '<i class="' . $val['icon'] . ' position-left"></i> ';
				}
				$str .= $val['name'] . '&nbsp;';
				$str .= '</li>';
			}
		}
		$str .= '</ol>';

		return $str;
	}
}