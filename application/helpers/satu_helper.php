<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
	function lang($key, $print = true) {
		$lang	= array();
		$lang[1]	= 'SISDA';
		$lang[2]	= 'Sistem Informasi Sumber Daya Air';
		$lang[3]	= 'Dinas PU Pemerintah Kabupaten MTB';
		$lang[4]	= 'powered by Technophoria Indonesia';
		$lang['skpd']	= 'Dinas Pekerjaan Umum';
		$lang['pemkab']	= 'Pemerintah Kabupaten Maluku Tenggara Barat';
		$lang['regional']	= 'Maluku Tenggara Barat';
		$lang['jalan']	= 'Jl. Ir. Soekarno - Saumlaki, Kabupaten Maluku Tenggara Barat';
		$lang['copy']	= 'Copyright &copy; 2017 Dinas Pekerjaan Umum Pemerintah Kabupaten Maluku Tenggara Barat';
		$lang['email']	= 'info@mtbkab.go.id';
		$lang['telpon']	= '(0918)';
		$lang[8]	= 'Data sukses disimpan';
		$lang[]	= 'Data Berhasil di Perbarui';
		$lang[]	= 'Perubahan berhasil disimpan !!!';
		$lang[]	= 'Perubahan data gagal dilakukan !!!';
		$lang[]	= 'Halaman tidak sah, silahkan menghubungi admin untuk informasi !!!';
		$lang[]	= 'Halaman tidak diperbolehkan !!!';
		$lang[]	= 'Form Harus Diisi, Data tidak lengkap !!!';
		$lang[15]	= 'Data berhasil dihapus';
		$lang[16]	= 'Data gagal dihapus';
		$lang[17]	= 'Data Tidak Tersedia !!!';
		$lang[22]	= 'Anda Berhasil Login';
		 $lang[55]   = 'Anda Berhasil Masuk ke Sistem';
		$lang[69]	= 'User atau Password salah !';
		$lang[]	= 'Data Sudah Tersedia !!!';
		$lang[]	= '<span class="glyphicon glyphicon-exclamation-sign"></span> Data Tidak Tersedia...';
		$lang[]	= '<span class="glyphicon glyphicon-exclamation-sign"></span> Data Tidak Lengkap !!!';
      $lang[]   = 'Anggota baru berhasil ditambahkan !';
      $lang[]   = '<span class="glyphicon glyphicon-exclamation-sign"></span> Data Belum Tersedia...';
      $lang[]   = 'Halaman tidak tersedia, silahkan pilih tanggal terlebih dahulu !';
      $lang[]   = 'Tidak Diperbolehkan... !!!';
      $lang['required']   = 'Formulir tidak boleh kosong...';
      $lang['min3']   = 'Formulir harus diisi minimal 3 karakter...';
		if( isset($lang[$key]) ) {
			if( $print == true ) {
				echo $lang[$key];
			} else {
				return $lang[$key];
			}
		}
	}

   //echo $decode = urlencode('e`miGhmocNiI`CeFmHaAmIiDki@oPi`AqHgi@sHkE_Bf@yh@jMmVjRir@dHkd@gIoZsP}[oc@}AaXgF{t@iVkh@ue@yO}\\i^{p@v[_yBdNoaCtZq_@{@}Ckj@kBakBag@imDglA_uIcf@ovAie@ccGlB{s@bRev@pA{k@yq@ovA_qBgsDsxC{rIozBsmPfCosEu@{{@uc@__Dmw@yxEke@icEvXwkBv`@wt@hL}w@cr@__L{KoaBi_@kmAsp@qjCiWmgHggCehTqmEq}[as@qpF{ImqB}PwzDwkA}~HbImpA`Me_Bof@ujDwl@ahBmAqgA{Okp@ys@sjChEcfEn^k{AeBwjA_tAomM}t@wvKux@chGm~@s{C{e@mxByn@q{@gi@ycEcQyu@s|@shAqo@omFiy@e|Cyx@myA}t@azDkw@gbCuBcqAiCojBi`@ebAeh@auAcm@}dBudCstGee@olCeaC{iOsc@sjCmjAo{EkhA_iHaa@goCg@uz@zYmgBsJk|BmpBqtXydBs~Uam@kcCka@erB|FamAgj@{tD}P_dGsZycRs^wdQ{Ky|GgCahG_Kgp@gAgk@~`@ceAfm@mfDvg@{qD|C_bC}w@meCia@koC{[odByb@kiAoj@gg@ilAi_Dcq@}}F}X}mIiUc~EsWmjEs`@ojBcWq_DgOyaCrJe`AsA}uBspAm{FyuBahHuNkcCvBgxBoPypAoW_jBiUgu@y_AwfBga@csAk_AatAoj@ov@c`Ak~AsnBai@_mBktAgcBkuAmcCgmC}bAaeAm^ilCrD_sBip@kfB}VqMyqAjB{d@oR}rBy~CycA{b@s^se@qxAm`DcbEwaHoxD}jFa}HyyJwZo}@kXi`B}~F{`Ku`B_cAgfCakEojGyjL{lB{pD_[mu@mhCckGsm@gsBgvDe`Mc`By`FifD__L{PcPy|@{PuS{Rw|AufFgsA_qEibAqiDycCekIcfCk|Jga@{`EfoAq}EnQqlF_LsxDqy@s}CoQwzGob@azBeyAy|EsjBg_FkrEe`Mi_AosAinAobEe_DsrKco@ii@y`BofE_jAudCuv@ciDslFajNqxDo}Esl@chA}kBw`Ge_DgcKmk@}aBwo@uaAmoAkfBs}@s~BoQ_hDq^gvAu[i]at@yu@{_@eeB_dAi`E{]oeDeq@ghF_eA}yFoJghAhBqhEpHk_BeF{wBhDuvE|Ao}AbNgj@jOcqAeDkWu_AaaC_|AefDmk@{ZiTaMy_@}r@_Tc^gp@anA}IiAmKoAiF`NkJcHRW');
   //die();

   function gAPIimg(){
      $key = 'AIzaSyCPmZMu4R8G507qRI3Z5wU6fe7NSiPzu1w'; //liekastrong static api
      $key = 'AIzaSyAh11hKRNC3x-JDlOZ7br-2TbEUSlkgX3o'; //tanahlautgmapapi static api
      return $key;
   }
   //

   function arr_datas($key = ''){
      $result = array(
               'Rawa' => 'Rawa',
               'Sungai' => 'Sungai',
               'Danau' => 'Danau',
               'Embung' => 'Embung',
               'Bendung' => 'Bendung',
               'Bendungan' => 'Bendungan',
               'Air Tanah' => 'Air Tanah',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_datas_slug($key = ''){
      $result = array(
               'rawa' => 'Rawa',
               'sungai' => 'Sungai',
               'danau' => 'Danau',
               'embung' => 'Embung',
               'bendung' => 'Bendung',
               'bendungan' => 'Bendungan',
               'air-tanah' => 'Air Tanah',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   //pengamanan_ltipe

   function pengamanan_ltipe($key = ''){
      $tdata = array('', 'Tanah','Bangunan Dan Gedung','Rumah Dinas','Kendaraan','KIB B dan KIB E','KIB D');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_pengamanans($key = ''){
      $result = array(
               'tanah' => 'Tanah',
               'bg' => 'Bangunan Dan Gedung',
               'rd' => 'Rumah Dinas',
               'kendaraan' => 'Kendaraan',
               'kib_be' => 'KIB B dan KIB E',
               'kib_d' => 'KIB D',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_pemanfaatan($key = ''){
      $result = array(
               //'sewa' => 'Sewa Aset',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_alldatas($key = ''){
      $result = array(
               'Rawa' => 'Rawa',
               'Sungai' => 'Sungai',
               'Danau' => 'Danau',
               'Embung' => 'Embung',
               'Bendung' => 'Bendung',
               'Bendungan' => 'Bendungan',
               'AirTanah' => 'Air Tanah',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_kib_tbstb($key = ''){
      $result = array(
               'kib_a' => 'TANAH DAN/ATAU BANGUNAN',
               'kib_b' => 'SELAIN TANAH DAN/ATAU BANGUNAN',
               'kib_c' => 'TANAH DAN/ATAU BANGUNAN',
               'kib_d' => 'SELAIN TANAH DAN/ATAU BANGUNAN',
               'kib_e' => 'SELAIN TANAH DAN/ATAU BANGUNAN',
               'kib_f' => 'SELAIN TANAH DAN/ATAU BANGUNAN',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_tbtsb($key = ''){
      $tdata = array('','TANAH DAN/ATAU BANGUNAN', 'SELAIN TANAH DAN/ATAU BANGUNAN');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_kondisikib($key = ''){
      $tdata = array('','Baik','Kurang Baik','Rusak Berat');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_jpemeliharaan($key = ''){
      $tdata = array('','Ringan','Sedang','Berat');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_taptus($key = ''){
      $tdata = array('','Status Pengguna', 'Sementara', 'Pengalihan', 'Dioperasikan');
      $tdata2 = array('','Status Pengguna', 'Sementara', 'Pengalihan', 'Dioperasikan Pihak Lain');
      $result = array_combine($tdata, $tdata2);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_status_pemanfaatan($key = ''){
      $tdata = array('','Berjalan','Selesai','Proses');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_pemanfaatan2($key = ''){
      $tdata = array('','Sewa Aset','Pinjam Pakai Aset','Kerja Sama Aset','Bangun Guna Serah','Bangun Serah Guna');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }



   function arr_data_referensi($key = ''){
      $result = array(
               'skpd' => 'SKPD',
               'jenis_badan' => 'Jenis Badan Usaha',
               'jenis_obyek' => 'Jenis Obyek',
               'status' => 'Status Obyek',
               'kecamatan' => 'Kecamatan',
               'desa' => 'Desa',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_referensi($key = ''){
      $result = array(
               'wilayah' => 'Wilayah',
               'jenis_ws' => 'Jenis WS',
               'jenis_rawa' => 'Jenis Rawa',
               'status' => 'Status Obyek',
               'kecamatan' => 'Kecamatan',
               'desa' => 'Desa',
               'mitra' => 'Pihak Ketiga',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_menureferensi($key = ''){
      $result = array(
               'wilayah' => 'Wilayah',
               'jenis_ws' => 'Jenis WS',
               'jenis_rawa' => 'Jenis Rawa',
               'jenis_sungai' => 'Jenis Sungai',
               'jenis_danau' => 'Jenis Danau',
               'jenis_embung' => 'Jenis Embung',
               'jenis_bendungan' => 'Jenis Bendungan',
               'jenis_air_tanah' => 'Jenis Air Tanah',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_laporan_referensi($key = ''){
      $result = array(
               'rumpun' => 'Rumpun Aplikasi',
               'skpd' => 'SKPD',
               'jenisaplikasi' => 'Jenis Aplikasi',
               'bahasapemrograman' => 'Bahasa Pemrograman',
               'database' => 'Database',
               'jenislayanan' => 'Jenis Layanan',
               'jenistenagaahli' => 'Jenis Tenaga Ahli',
               'penggunalayanan' => 'Pengguna Layanan',
               'perangkatjaringan' => 'Jenis Perangkat Jaringan',
               'perangkatkeras' => 'Jenis Perangkat Keras',
               'perangkatkhusus' => 'Jenis Perangkat Khusus',
               'perangkatlunak' => 'Jenis Perangkat Lunak',
            );
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function arr_yn($key = ''){
      $tdata = array('Tidak', 'Ya');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_sb($key = ''){
      $tdata = array('Belum', 'Sudah');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_kondisi($key = ''){
      $tdata = array('','B', 'KB', 'RB');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }
   function arr_tipe($key = ''){
      $tdata = array('','Sewa Aset','Pinjam Pakai Aset','Kerja Sama Aset','Bangun Guna Serah','Bangun Serah Guna');
      $result = array_combine($tdata, $tdata);
		if(isset($key) and $key) {
		   if(isset($result[$key])){
   			return $result[$key];
		   } else {
		      return false;
		   }
		} else {
			return $result;
		}
   }

   function field_tipe($key = ''){
      $result = array(
               'text' => 'Text',
               'number' => 'Number',
               'select' => 'Select',
               'textarea' => 'Textarea',
               'radio' => 'Radio',
               'checkbox' => 'Checkbox',
               'date' => 'Date',
               'hidden' => 'Hidden',
               'time' => 'Time'
            );
		if(isset($result[$key])) {
			return $result[$key];
		} else {
			return $result;
		}
   }

   function fonts(){
      $dir = FCPATH .'assets/fonts';
      $mapf = directory_map($dir);
      echo '<style type="text/css">';
      foreach($mapf as $vmap){
         $xmap = explode('-', $vmap);
         $repmap = trim(str_replace(array('-','.ttf'), ' ', $vmap));
         $repmap2 = trim(str_replace('.ttf', ' ', $vmap));
      ?>
         @font-face {
           font-family: '<?php echo $xmap[0]; ?><?php //echo $repmap; ?>';
           font-style: normal;
           src: local('<?php echo $repmap; ?>'), local('<?php echo $repmap2; ?>'), url('<?php echo base_url('assets/fonts/'. $vmap); ?>') format('truetype');
         }
      <?php
      }
      echo '</style>';
   }



   function joiner_ref($table, $key = '', $depantabel = 'ref_', $lr = 'left'){
      if($depantabel == '?') $depantabel = '';
      $return = array($depantabel . $key, $table .'.'. $key .'_id = '. $depantabel . $key .'.'. $key .'_id', $lr);
      //print_r($return);die();
      return $return;
   }

   function mm_joiner($tableinduk, $table, $keyinduk = '', $lr = '', $keychild = ''){
      if(!$keychild){ $keychild = $keyinduk; }
      $return = array($table, $tableinduk .'.'. $keyinduk .' = '. $table .'.'. $keychild .'', $lr);
      //print_r($return);die();
      return $return;
   }

   function joiner_array($table, $key = '', $depantabel = 'ref_', $lr = ''){
      if($depantabel == '?') $depantabel = '';
      $return = array($depantabel . $key, $table .'.'. $key .'_id = '. $depantabel . $key .'.'. $key .'_id', $lr);
      //print_r($return);die();
      return $return;
   }
   function joiner_arr($key, $table = '', $lr = 'left'){
      $return = array();
      if(is_array($key)){
         foreach($key as $kkey => $vkey){
            $return[] = array('ref_'. $kkey, $vkey .'.'. $kkey .'_id = ref_'. $kkey .'.'. $kkey .'_id', $lr);
         }
      } else {
         //$return = array('ref_'. $key, $table .'.'. $key .'_id = ref_'. $key .'.'. $key .'_id', $lr);
      }
      return $return;
   }

   function arr_kasus_tipe($k = ''){
		$result	= array();
      $result['Dewasa'] = 'Dewasa';
      $result['Anak-anak'] = 'Anak-anak';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
   }

   function arr_kasus_jenis($k = ''){
		$result	= array();
      $result['B'] = 'Baru';
      $result['U'] = 'Berulang';
      $result['R'] = 'Rujukan';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
   }

   function arr_triwulan($k = ''){
		//$result	= array('1' => "TRIWULAN I", "TRIWULAN II", "TRIWULAN III", "TRIWULAN IV" );
		$result	= array();
      $result[1] = 'Triwulan I';
      $result[] = 'Triwulan II';
      $result[] = 'Triwulan III';
      $result[] = 'Triwulan IV';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
   }

   function get_triwulan_by_bulan($k = ''){
		$result	= array();
      $result[1] = 1;
      $result[] = 1;
      $result[] = 1;
      $result[4] = 2;
      $result[] = 2;
      $result[] = 2;
      $result[7] = 3;
      $result[] = 3;
      $result[] = 3;
      $result[10] = 4;
      $result[] = 4;
      $result[] = 4;
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
   }

   function arr_semester($k = ''){
		$result	= array();
      $result[1] = 'Semester I';
      $result[] = 'Semester II';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
   }

   function get_semester_by_bulan($k = ''){
		$result	= array();
      $result[1] = 1;
      $result[] = 1;
      $result[] = 1;
      $result[] = 1;
      $result[] = 1;
      $result[] = 1;
      $result[7] = 2;
      $result[] = 2;
      $result[] = 2;
      $result[] = 2;
      $result[] = 2;
      $result[] = 2;
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
   }

	function arr_umur( $k = '' ){
		$result	= array();
        $result[0] = 'Tidak Diketahui';
        $result['1-14'] = '1-14';
        $result['15-20'] = '15-20';
        $result['21-26'] = '21-26';
        $result['27-32'] = '27-32';
        $result['33-38'] = '33-38';
        $result['39-44'] = '39-44';
        $result['45-50'] = '45-50';
        $result['51-56'] = '51-56';
        $result['57-62'] = '57-62';
        $result['63-68'] = '63-68';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function arr_umur_laporan( $k = '' ){
		$result	= array();
        $result[0] = 'Tidak Diketahui';
        $result[1] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[] = '1-14';
        $result[15] = '15-20';
        $result[] = '15-20';
        $result[] = '15-20';
        $result[] = '15-20';
        $result[] = '15-20';
        $result[] = '15-20';
        $result[21] = '21-26';
        $result[] = '21-26';
        $result[] = '21-26';
        $result[] = '21-26';
        $result[] = '21-26';
        $result[] = '21-26';
        $result[] = '21-26';
        $result[27] = '27-32';
        $result[] = '27-32';
        $result[] = '27-32';
        $result[] = '27-32';
        $result[] = '27-32';
        $result[] = '27-32';
        $result[] = '27-32';
        $result[33] = '33-38';
        $result[] = '33-38';
        $result[] = '33-38';
        $result[] = '33-38';
        $result[] = '33-38';
        $result[] = '33-38';
        $result[39] = '39-44';
        $result[] = '39-44';
        $result[] = '39-44';
        $result[] = '39-44';
        $result[] = '39-44';
        $result[] = '39-44';
        $result[45] = '45-50';
        $result[] = '45-50';
        $result[] = '45-50';
        $result[] = '45-50';
        $result[] = '45-50';
        $result[] = '45-50';
        $result[51] = '51-56';
        $result[] = '51-56';
        $result[] = '51-56';
        $result[] = '51-56';
        $result[] = '51-56';
        $result[] = '51-56';
        $result[57] = '57-62';
        $result[] = '57-62';
        $result[] = '57-62';
        $result[] = '57-62';
        $result[] = '57-62';
        $result[] = '57-62';
        $result[63] = '63-68';
        $result[] = '63-68';
        $result[] = '63-68';
        $result[] = '63-68';
        $result[] = '63-68';
        $result[] = '63-68';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function arr_umur_rekap( $k = '' ){
		$result	= array();
        $result[0] = '0-17';
        $result[1] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[] = '0-17';
        $result[18] = '18-24';
        $result[] = '18-24';
        $result[] = '18-24';
        $result[] = '18-24';
        $result[] = '18-24';
        $result[] = '18-24';
        $result[] = '18-24';
        $result[25] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';
        $result[] = '25+';

		if(isset($result[$k]) AND $result[$k]) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function arr_kelamin( $k = '' ){
		$result	= array();
        $result['Laki-laki'] = 'Laki-laki';
        $result['Perempuan'] = 'Perempuan';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
   //('Anak Kandung', 'Tiri', 'Keponakan')
	function arr_pernikahan( $k = '' ){
		$result	= array();
        $result['Belum Kawin'] = 'Belum Kawin';
        $result['Kawin'] = 'Kawin';
        $result['Cerai'] = 'Cerai';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function arr_kebangsaan( $k = '' ){
		$result	= array();
        $result['WNI'] = 'WNI';
        $result['WNA'] = 'WNA';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function arr_status_korban( $k = '' ){
		$result	= array();
        $result['Anak Kandung'] = 'Anak Kandung';
        $result['Tiri'] = 'Tiri';
        $result['Keponakan'] = 'Keponakan';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function jenis_idcard( $k = '' ){
		$result	= array();
        $result[1] = 'KTP';
        $result[] = 'SIM';
        $result[] = 'Kartu Pelajar';
        $result[] = 'Kartu Mahasiswa';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
   function ex_select($datas){
      /*?><option value=""></option><?php */
      foreach($datas as $kcombo => $vcombo){ ?><option value="<?php echo $kcombo; ?>"><?php echo $vcombo; ?></option><?php }
   }
	function ar_status_nikah( $k = '' ){
		$result	= array();
        $result['Menikah'] = 'Menikah';
        $result['Belum Menikah'] = 'Belum Menikah';
        $result['Janda'] = 'Janda';
        $result['Duda'] = 'Duda';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function ar_identitas( $k = '' ){
		$result	= array();
        $result['KTP'] = 'KTP';
        $result['SIM'] = 'SIM';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function ar_kelamin( $k = '' ){
		$result	= array();
        $result['L'] = 'L';
        $result['P'] = 'P';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function ar_agama( $k = '' ){
		$result	= array();
        $result['Islam'] = 'Islam';
        $result['Kristen Protestan'] = 'Kristen Protestan';
        $result['Kristen Katolik'] = 'Kristen Katolik';
        $result['Hindu'] = 'Hindu';
        $result['Budha'] = 'Budha';
        $result['Khonghucu'] = 'Khonghucu';
        $result['Lainnya'] = 'Lainnya';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function ar_goldar( $k = '' ){
		$result	= array();
        $result['A'] = 'A';
        $result['B'] = 'B';
        $result['O'] = 'O';
        $result['AB'] = 'AB';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function ar_status_pekerjaan( $k = '' ){
		$result	= array();
        $result[] = '';
        $result[] = 'PNS';
        $result[] = 'CPNS';
        $result[] = 'Tetap';
        $result[] = 'Tidak Tetap';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function ar_status_pegawai( $k = '' ){
		$result	= array();
        $result[] = '';
        $result[] = 'Aktif';
        $result[] = 'Ijin Belajar';
        $result[] = 'Luar Kota';
        $result[] = 'Pensiun';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
    function listDataJenisWaktuTahun($k = ''){
        $data = array();
        $data['1|2|3|4|5|6']    = 'Semester I';
        $data['7|8|9|10|11|12']    = 'Semester II';
        $data['1|2|3']    = 'Semester I';
        if($k AND isset($data[$k]) AND $data[8]){
            return $data[$k];
        } else {
            return $data;
        }
    }
   function make_ar_key_val($key, $val, $ardatas){
      $returndata = array();
      foreach($ardatas as $tdata){
         $returndata[$tdata[$key]] = $tdata[$val];
      }
      return $returndata;
   }
	function sess( $se = null ) {
		$CI =& get_instance();
        if($se == null)
            return $CI->session->all_userdata();
		//$this->load->library('session');
		$return = $CI->session->userdata($se);
		if($return)
			return $return;
		else
			return false;
	}
	function sessf( $se = null ) {
		$CI =& get_instance();
        if($se == null)
            return $CI->session->all_userdata();
		//$this->load->library('session');
		$return = $CI->session->flashdata($se);
		if($return)
			return $return;
		else
			return false;
	}
	function is_superadmin(){
		return (sess('level') >= 9) ? true : false;
	}
	function is_admin(){
		return (sess('level') >= 7) ? true : false;
	}
	function is_verifikator(){
		return (sess('level') >= 5) ? true : false;
	}
	function is_operator(){
		return (sess('level') >= 3) ? true : false;
	}
	function this_operator(){
		return (sess('level') == 3) ? true : false;
	}
	function is_anggota(){
		return (sess('level') >= 1) ? true : false;
	}
	function is_user(){
		return (sess('level') >= 1) ? true : false;
	}
	function is_login(){
		return (sess('logged_in') >= 1) ? true : false;
	}
   function user_level($level = ''){
		$result	= array();
        //$result[0] = '';
        $result[3] = 'Operator';
        //$result[6] = 'Verifikator';
        //$result[7] = 'Admin';
        $result[9] = 'Administrator';
		if(isset($result[$level]) and $result[$level]) {
			return $result[$level];
		} else {
			return $result;
		}
   }
   function user_status($k = ''){
		$result	= array();
        $result['Aktif'] = 'Aktif';
        $result['Tidak Aktif'] = 'Tidak Aktif';
		if(isset($result[$k]) and $result[$k]) {
			return $result[$k];
		} else {
			return $result;
		}
   }
	function is_serialized( $data ) {
		// if it isn't a string, it isn't serialized
		if ( !is_string( $data ) )
			return false;
		$data = trim( $data );
		if ( 'N;' == $data )
			return true;
		if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
			return false;
		switch ( $badions[1] ) {
			case 'a' :
			case 'O' :
			case 's' :
				if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
					return true;
				break;
			case 'b' :
			case 'i' :
			case 'd' :
				if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
					return true;
				break;
		}
		return false;
	}
	function SchInt( $int ) {
		if ( preg_match('/^[0-9]+$/i', $int, $m) ) {
			return $int;
		} else {
			return false;
		}
	}

	function arr_bln( $k = '' ){
		$result	= array('1' => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" );
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function arr_bln2( $k = '' ){
		$result	= array('1' => "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nop", "Des" );
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function arr_hari( $k = '' ){
		$result	= array('1' => "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu" );
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function make_arr_hari( $k = '' ){
		$result	= array('1' => "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" );
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
	}
	function SchTgl( $date = '', $fullmonth = true, $lihathari = true, $lihatjam = true ) {
        if(!$date)
            $date = date('Y-m-d H:i:s');
		$arr_bln	= array("", "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des" );
		if($fullmonth)
         $arr_bln	= array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" );
		$arr_hari	= array("", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu" );
		if ( $date == '0000-00-00' ) {
			return '';
		} else {
			$date	= trim( $date );
			$hari	= intval(date('N', strtotime($date)));
			$date	= str_replace(array(' ', ':'), '-', $date);
			//@list($th, $bl, $tg)	= explode('-', $date);
			@list($th, $bl, $tg, $jam, $min, $sec)	= explode('-', $date);
			$tgl_indo	= '';
			if($lihathari == true)
				$tgl_indo	.= ' '. $arr_hari[$hari].', ';
			$tgl_indo	.= $tg .' '.$arr_bln[intval($bl)].' '.$th;
            if($lihatjam == true AND $jam)
                $tgl_indo .=  ' '. $jam .':'. $min;
			return $tgl_indo;
		}
	}
	function fSchTgl( $date = '', $format = '', $fullmonth = true, $lihathari = true, $lihatjam = true ) {
        if(!$date)
            $date = date('Y-m-d H:i:s');
		$arr_bln	= array("", "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des" );
		if($fullmonth)
         $arr_bln	= array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" );
		$arr_hari	= array("", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu" );
		if ( $date == '0000-00-00' ) {
			return '';
		} elseif ( $date == '0000-00-00 00:00:00' ) {
			return '';
		} else {
			$date	= trim( $date );
			$hari	= intval(date('N', strtotime($date)));
			$date	= str_replace(array(' ', ':'), '-', $date);
			//@list($th, $bl, $tg)	= explode('-', $date);
			@list($th, $bl, $tg, $jam, $min, $sec)	= explode('-', $date);
			$tgl_indo	= '';

         if($format){
            $tgl_indo = $format;
            /*$tgl_indo = str_replace('hari', $arr_hari[$hari], $tgl_indo);
            $tgl_indo = str_replace('tahun', $th, $tgl_indo);
            $tgl_indo = str_replace('bulan', $arr_bln[intval($bl)], $tgl_indo);
            $tgl_indo = str_replace('tanggal', $tg, $tgl_indo);
            $tgl_indo = str_replace('jam', $jam, $tgl_indo);
            $tgl_indo = str_replace('menit', $min, $tgl_indo);
            $tgl_indo = str_replace('detik', $sec, $tgl_indo);*/

            $tgl_indo = str_replace('hri', $arr_hari[$hari], $tgl_indo);
            $tgl_indo = str_replace('thn', $th, $tgl_indo);
            $tgl_indo = str_replace('bln', $arr_bln[intval($bl)], $tgl_indo);
            $tgl_indo = str_replace('tgl', $tg, $tgl_indo);
            $tgl_indo = str_replace('jam', $jam, $tgl_indo);
            $tgl_indo = str_replace('mnt', $min, $tgl_indo);
            $tgl_indo = str_replace('dtk', $sec, $tgl_indo);
         } else {
   			if($lihathari == true){
      		   $tgl_indo	.= ' '. $arr_hari[$hari].', ';
   			}
			   $tgl_indo	.= $tg .' '.$arr_bln[intval($bl)].' '.$th;
            if($lihatjam == true AND $jam){
               $tgl_indo .=  ' '. $jam .':'. $min;
            }
         }
			return $tgl_indo;
		}
	}
	function SchTglJam( $date = '', $lihathari = true, $lihatjam = true ) {
        if(!$date)
            $date = date('Y-m-d H:i:s');
		$arr_bln	= array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember" );
		$arr_hari	= array("", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu" );
		if ( $date == '0000-00-00 00:00:00' ) {
			return '';
		} else {
			$date	= trim( $date );
			$hari	= intval(date('N', strtotime($date)));
			$date	= str_replace(array(' ', ':'), '-', $date);
			@list($th, $bl, $tg, $jam, $min, $sec)	= explode('-', $date);
			$tgl_indo	= '';
			if($lihathari == true)
				$tgl_indo	.= ' '. $arr_hari[$hari].', ';
			$tgl_indo	.= $tg .' '.$arr_bln[intval($bl)].' '.$th;
            if($lihatjam == true AND $jam)
                $tgl_indo .=  ' '. $jam .':'. $min;
			return $tgl_indo;
		}
	}
	function SchTglEn( $date = '', $lihathari = true ) {
        if(!$date)
            $date = date('Y-m-d H:i:s');
		$arr_bln	= array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" );
		$arr_hari	= array("", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu" );
		if ( $date == '0000-00-00 00:00:00' ) {
			return '';
		} else {
			$date	= trim( $date );
			$hari	= intval(date('N', strtotime($date)));
			$date	= str_replace(array(' ', ':'), '-', $date);
			list($th, $bl, $tg, $jam, $min, $sec)	= explode('-', $date);
			$tgl_indo	= '';
			if($lihathari == true)
				$tgl_indo	.= ' '. $arr_hari[$hari].', ';
			$tgl_indo	.= $tg .' '.$arr_bln[intval($bl)].' '.$th .' '. $jam .':'. $min;
			return $tgl_indo;
		}
	}
	function SchTglEnTJ( $date = '', $lihathari = true ) {
        if(!$date)
            $date = date('Y-m-d H:i:s');
		$arr_bln	= array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" );
		$arr_hari	= array("", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu" );
		if ( $date == '0000-00-00 00:00:00' ) {
			return '';
		} else {
			$date	= trim( $date );
			$hari	= intval(date('N', strtotime($date)));
			$date	= str_replace(array(' ', ':'), '-', $date);
			list($th, $bl, $tg)	= explode('-', $date);
			$tgl_indo	= '';
			if($lihathari == true)
				$tgl_indo	.= ' '. $arr_hari[$hari].', ';
			$tgl_indo	.= $tg .' '.$arr_bln[intval($bl)].' '.$th;
			return $tgl_indo;
		}
	}
    function SchRp($num){
        if(!$num)
            return '-';
        $format = number_format($num, 0, '', '.');
        return $format;
    }
    function SchPhone($num){
        if(!$num)
            return '-';
        $format = number_format($num, 0, '', '-');
        return $format;
    }
if ( !function_exists('SchCleanChar') ){
	function SchCleanChar($result) {
		$result = strip_tags($result);
		$result = preg_replace('/&.+?;/', ' ', $result);
		$result = preg_replace('/\s+/', ' ', $result);
		$result = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', ' ', $result);
		$result = preg_replace('|-+|', ' ', $result);
		$result = preg_replace('/&#?[a-z0-9]+;/i', ' ',$result);
		$result = preg_replace('/[^%A-Za-z0-9 _-]/', ' ', $result);
		$result = trim($result, ' ');
		$result	= str_replace(array("     ", "     ", "    ", "   ", "  ", "--", " "), " ", $result);
		return $result;
	}
}
	function SchCleanInt($result) {
		$result = strip_tags($result);
		$result = preg_replace('/&.+?;/', ' ', $result);
		$result = preg_replace('/\s+/', ' ', $result);
		$result = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', ' ', $result);
		$result = preg_replace('|-+|', ' ', $result);
		$result = preg_replace('/&#?[0-9]+;/i', ' ',$result);
		$result = preg_replace('/[^%0-9]/', ' ', $result);
		$result = trim($result, ' ');
		$result	= str_replace(array("     ", "     ", "    ", "   ", "  ", "--", " "), "", $result);
		return $result;
	}

   function atozz($lower = 'B', $upper = 'ZZ') {
       ++$upper;
       $data = array();
       $data[1] = 'A';
       for ($i = $lower; $i !== $upper; ++$i) {
           $data[] = $i;
       }
      return $data;
   }

if ( !function_exists('to_slug') ){
	function to_slug( $title ) {
		$title			= trim($title);
		$arra			= array ("!", "@", "/", ":", "#", "$", "%", "^", "&", "*", "(", ")", " - ", "_", "?", ">", "<", ",", "[", "]", "{", "}");
		$clean_title	= str_replace($arra, " ", $title);
		$arra2			= array ("     ", "    ", "   ", "  ", "---", "--", " ");
		$_tolink		= strtolower(str_replace($arra2, "-", trim($clean_title)));
		return $_tolink;
	}
}
if ( !function_exists('title_to_link') ){
	function title_to_link ( $title ) {
		$title			= trim($title);
		$arra			= array ("!", "@", "/", ":", "#", "$", "%", "^", "&", "*", "(", ")", " - ", "_", "?", ">", "<", ".", ",", "[", "]", "{", "}");
		$clean_title	= str_replace($arra, " ", $title);
		$arra2			= array ("     ", "    ", "   ", "  ", "--", " ");
		$_tolink		= strtolower(str_replace($arra2, "-", trim($clean_title)));
		return $_tolink;
	}
}
function formatCount($n, $singular, $plural, $none = '0')
{
	if ($n == 0) {
		return "{$none}&nbsp;{$plural}";
	} elseif ($n == 1) {
		return "{$n}&nbsp;{$singular}";
	} else {
		return "{$n}&nbsp;{$plural}";
	}
}
function waktuin( $from, $to = '' ) {
	if ( empty($to) )
		$to = time();
	$diff = (int) abs($to - $from);
	if ($diff <= 3600) {
		$mins = round($diff / 60);
		if ($mins <= 1) {
			$mins = 1;
		}
		/* translators: min=minute */
		$since = formatCount($mins, 'menit', 'menit');
	} else if (($diff <= 86400) && ($diff > 3600)) {
		$hours = round($diff / 3600);
		if ($hours <= 1) {
			$hours = 1;
		}
		$since = formatCount($hours, 'jam', 'jam');
	} elseif ($diff < 2592000 && ($diff > 86400)) {
		$days = round($diff / 86400);
		if ($days <= 1) {
			$days = 1;
		}
		$since = formatCount($days, 'hari', 'hari');
	} else if (($diff >= 2592000)) {
		$bln = round($diff / 2592000);
		if ($bln <= 1) {
			$bln = 1;
		}
		$since = formatCount($bln, 'bulan', 'bulan');
	}
    //2592000
	$formatted = (($to-$from) < 0) ? ("dalam ".$since) : ($since." yang lalu");
	return $formatted;
}
//tambahan hendrax
function pesan_warning($jenis,$pesan)
{
   if ($jenis=='success') {
      $jdl = 'Sukses';
      $icn = 'check';
   }
   else if ($jenis=='warning') {
      $jdl = 'Peringatan';
      $icn = 'warning';
   }
   else if ($jenis=='danger') {
      $jdl = 'Kesalahan';
      $icn = 'exclamation-circle';
   }
   else {
      $jdl = 'Informasi';
      $icn = 'info-circle';
   }
   $a = '<div class="alert alert-'.$jenis.' alert-dismissible animated slideInUp ">
            <i class="fa fa-'.$icn.' fa-fw fa-2x pull-left"></i>
            <button class="close" data-dismiss="alert">&times;</button>
            <b>'.$jdl.'</b><br>'.$pesan.'.
         </div>';
   return $a;
}

function count_umur($tanggal, $sampaitanggal = 'today', $tipe='tahun')
{
   // PHP 5.3 +
   if($tanggal == '0000-00-00') return 0;
   if(!$tanggal) return 0;

   $a = date_create($tanggal);
   $b = date_create($sampaitanggal);
   if ($tipe=='tahun') {
      return $a->diff($b)->y;
   }
   else if ($tipe=='bulan') {
      return $a->diff($b)->y.' tahun '.$a->diff($b)->m.' bulan';
   }
   else if ($tipe=='thn-bln') {
      return $a->diff($b)->y.'-'.$a->diff($b)->m;
   }
   else {
      return '<strong>'.$a->diff($b)->y.'</strong> thn <strong>'.$a->diff($b)->m.'</strong> bln <strong>'.$a->diff($b)->d.'</strong> hari';
   }
}
function count_mkg($mkg,$tmt)
{
   if ($mkg && $tmt!='0000-00-00') {
      list($mkg_thn,$mkg_bln) = explode('-',$mkg);
      list($sel_thn,$sel_bln) = explode('-',count_umur($tmt,'thn-bln'));
      $mkg_t = $mkg_thn + $sel_thn;
      $mkg_b = $mkg_bln + $sel_bln;
      $tahun = ($mkg_b >= 12) ? $mkg_t + 1 : $mkg_t;
      $bulan = ($mkg_b >= 12) ? ($mkg_bln + $sel_bln) - 12 : $mkg_b;
      //return $mkg_t.'-'.$mkg_b.'<br>'.$tahun.' tahun '.$bulan.' bulan';
      return $tahun.' tahun '.$bulan.' bulan';
   }
   else {
      return '-';
   }
}
function nama_pegawai($gelar_d,$nama,$gelar_b)
{
   $r  = ($gelar_d) ? ucwords($gelar_d).' ' : '';
   $r .= ucwords($nama);
   $r .= ($gelar_b) ? ', '.ucwords($gelar_b) : '';
   return $r;
}
function tgl_excel_to_mysql($tanggal)
{
    $thn = ''; $bln = '';$tgl = '';
   @list($tgl,$bln,$thn) = explode('/',$tanggal);
   return $thn.'-'.$bln.'-'.$tgl;
}
function tgl_indo_to_mysql($tanggal)
{
    $thn = ''; $bln = '';$tgl = '';
   @list($tgl,$bln,$thn) = explode('-',$tanggal);
   return $thn.'-'.$bln.'-'.$tgl;
}
function tgl_mysql_to_indo($tanggal)
{
    $thn = ''; $bln = '';$tgl = '';
   @list($thn,$bln,$tgl) = explode('-',$tanggal);
   return $tgl.'-'.$bln.'-'.$thn;
}
function cek_tgl_indo($tanggal)
{
   // 20-02-2015
   if (substr($tanggal,2,1)=='-' && substr($tanggal,5,1)=='-') {
      return TRUE;
   }
   else {
      return FALSE;
   }
}
//end hendrax
/*
 * FUNGSI NUMERIK KE TERHITUNG
 * (c) 2008-2010 by amarullz@yahoo.com
 *
 */
function terbilang_get_valid($str,$from,$to,$min=1,$max=9){
	$val=false;
	$from=($from<0)?0:$from;
	for ($i=$from;$i<$to;$i++){
		if (((int) $str{$i}>=$min)&&((int) $str{$i}<=$max)) $val=true;
	}
	return $val;
}
function terbilang_get_str($i,$str,$len){
	$numA=array("","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan");
	$numB=array("","se","dua ","tiga ","empat ","lima ","enam ","tujuh ","delapan ","sembilan ");
	$numC=array("","satu ","dua ","tiga ","empat ","lima ","enam ","tujuh ","delapan ","sembilan ");
	$numD=array(0=>"puluh",1=>"belas",2=>"ratus",4=>"ribu", 7=>"juta", 10=>"milyar", 13=>"triliun");
	$buf="";
	$pos=$len-$i;
	switch($pos){
		case 1:
				if (!terbilang_get_valid($str,$i-1,$i,1,1))
					$buf=$numA[(int) $str{$i}];
			break;
		case 2:	case 5: case 8: case 11: case 14:
				if ((int) $str{$i}==1){
					if ((int) $str{$i+1}==0)
						$buf=($numB[(int) $str{$i}]).($numD[0]);
					else
						$buf=($numB[(int) $str{$i+1}]).($numD[1]);
				}
				else if ((int) $str{$i}>1){
						$buf=($numB[(int) $str{$i}]).($numD[0]);
				}
			break;
		case 3: case 6: case 9: case 12: case 15:
				if ((int) $str{$i}>0){
						$buf=($numB[(int) $str{$i}]).($numD[2]);
				}
			break;
		case 4: case 7: case 10: case 13:
				if (terbilang_get_valid($str,$i-2,$i)){
					if (!terbilang_get_valid($str,$i-1,$i,1,1))
						$buf=$numC[(int) $str{$i}].($numD[$pos]);
					else
						$buf=$numD[$pos];
				}
				else if((int) $str{$i}>0){
					if ($pos==4)
						$buf=($numB[(int) $str{$i}]).($numD[$pos]);
					else
						$buf=($numC[(int) $str{$i}]).($numD[$pos]);
				}
			break;
	}
	return $buf;
}
function toTerbilang($nominal){
	$buf="";
	$str=$nominal."";
	$len=strlen($str);
	for ($i=0;$i<$len;$i++){
		$buf=trim($buf)." ".terbilang_get_str($i,$str,$len);
	}
	return ucwords(trim($buf)) .' Rupiah';
}

function userlevel( $k = '' ){
		$result	= array();
        $result[9] = 'Superadmin';
        $result[7] = 'Admin';
        $result[6] = 'Verifikator';
        $result[5] = 'Operator';
		if($k) {
			return $result[$k];
		} else {
			return $result;
		}
}


   function arr_formg($data){
      $ndata = array();
      foreach($data as $d => $v){
         $nlab = trim(ucwords(str_replace(array('_', 'id'), ' ', $d)));
         $ndata = "\t\t\t\t\t'". $d ."' => array(
                        'table' => 1,
                        'label' => '". $nlab ."',
                        'label_class' => 'col-sm-2 control-label',
                        'form' => 1,
                        'form_before' => '<div class=\"col-sm-10\">',
                        'form_label' => '". $nlab ."',
                        'form_type' => 'text',
                        'form_name' => 'data[". $d ."]',
                        'form_class' => 'form-control underline',
                        'form_id' => '". $d ."',
                        'form_value' => '',
                        'form_after' => '</div>',
                        'keyvaldata' => array(),
                        'attr' => '',
                        'before' => '',
                        'after' => ''
                     ),\n";
         /*$ndata = "\t\t\t\t\t\t\"". $d .'" => array(
                        "table" => 1,
                        "label" => "'. $nlab .'",
                        "label_class" => "col-sm-2 control-label",
                        "form" => 1,
                        "form_label" => "data['. $d .']",
                        "form_type" => "text",
                        "form_name" => "form-control underline",
                        "form_class" => "form-control underline",
                        "form_id" => "'. $d .'",
                        "form_col" => "col-sm-10",
                        "keyvaldata" => array(),
                        "before" => "",
                        "after" => "",
                     ),'. "\n";*/
         print_r($ndata);
      }
         die();
   }

   function arr_formv($data){
      $ndata = array();
      foreach($data as $d => $v){
         $nlab = trim(ucwords(str_replace(array('_', 'id'), ' ', $d)));
         $ndata = "\t\t\t\t\t'". $d ."' => array(
                        'table' => 1,
                        'label' => '". $nlab ."',
                        'label_class' => 'col-sm-2 control-label',
                        'form' => 1,
                        'form_before' => '<div class=\"col-sm-10\">',
                        'form_label' => '". $nlab ."',
                        'form_type' => 'text',
                        'form_name' => 'data[". $d ."]',
                        'form_class' => 'underline',
                        'form_id' => '". $d ."',
                        'form_value' => data[\'". $d ."\'],
                        'form_after' => '</div>',
                        'keyvaldata' => array(),
                        'attr' => '',
                        'before' => '',
                        'after' => ''
                     ),\n";
         print_r($ndata);
      }
         die();
   }

   function obyek_details($data = array()){
      $dt = array();
      $dt['SKPD'] = '<strong>'. $data['skpd_nama'] .'</strong>';
      $dt['Jenis Obyek'] = '<strong>'. $data['jenis_obyek_nama'] .'</strong>';
      $dt['Nama Obyek'] = $data['obyek_nama'];
      $dt['Lokasi'] = $data['obyek_lokasi'];
      $dt['Kecamatan'] = $data['kecamatan_nama'];
      $dt['Luas Tanah'] = $data['obyek_ltanah'];
      $dt['Luas Bangunan'] = $data['obyek_lbangunan'];
      $dt['Keterangan'] = $data['obyek_keterangan'];

      /*
      $dt[''] = $data[''];
      $dt[''] = $data[''];*/

      return $dt;
   }

   function pemanfaatan_details($data = array()){
      $dt = array();
      /*$dt['SKPD Pemilik'] = '<strong>'. $data['skpd_nama'] .'</strong>';
      $dt['Jenis Obyek'] = '<strong>'. $data['jenis_obyek_nama'] .'</strong>';
      $dt['Nama Obyek'] = $data['obyek_nama'];
      $dt['Lokasi'] = $data['obyek_lokasi'];
      $dt['Kecamatan'] = $data['kecamatan_nama'];
      $dt['Luas Tanah Obyek'] = $data['obyek_ltanah'];
      $dt['Luas Bangunan Obyek'] = $data['obyek_lbangunan'];
      $dt['Keterangan Obyek'] = $data['obyek_keterangan'];*/
      $dt['<strong>Pemanfaatan Tipe</strong>'] = '<strong>'. $data['pemanfaatan_tipe'] .'</strong>';
      $dt['Pihak Ketiga'] = $data['mitra_nama'];
      $dt['Sertifikat'] = $data['pemanfaatan_sertifikat'];
      $dt['Tanggal Mulai'] = SchTgl($data['pemanfaatan_mulai']);
      $dt['Tanggal Selesai'] = SchTgl($data['pemanfaatan_selesai']);
      $dt['Luas Tanah'] = $data['pemanfaatan_ltanah'];
      $dt['Luas Bangunan'] = $data['pemanfaatan_lbangunan'];
      $dt['Keterangan Pemanfaatan'] = $data['pemanfaatan_keterangan'];

      /*
      $dt[''] = $data[''];
      $dt[''] = $data[''];*/

      return $dt;
   }

   function aset_details($data = array()){
      $dt = array();
      $dt['SKPD'] = '<strong>'. $data['skpd_nama'] .'</strong>';
      $dt['Jenis Obyek'] = '<strong>'. $data['jenis_obyek_nama'] .'</strong>';
      $dt['Nama Obyek'] = $data['obyek_nama'];
      $dt['Lokasi'] = $data['obyek_lokasi'];
      $dt['Kecamatan'] = $data['kecamatan_nama'];
      $dt['Luas Tanah'] = $data['obyek_ltanah'];
      $dt['Luas Bangunan'] = $data['obyek_lbangunan'];
      $dt['Keterangan'] = $data['obyek_keterangan'];

      /*
      $dt[''] = $data[''];
      $dt[''] = $data[''];*/

      return $dt;
   }

if(!function_exists('_credit')){
	function _credit(){
      $creds = '';
      $creds .= '<div class="container-fluid">';
      $creds .= '<div class="row">';
      $creds .= '<div class="col-md-12 text-right">';
      $creds .= '<a href="http://technophoria.co.id" target="_blank">';
      $creds .= '<img src="'. base_url('assets/images/logo-technophoria-indonesia.png') .'" style="max-height: 100%;" class="hidden-sm hidden-xs" alt="Sistem Informasi by Technophoria Indonesia" title="Sistem Informasi by Technophoria Indonesia" />';
      $creds .= '<span class="visible-sm visible-xs fs10 ls1">'. lang('_credit', false) .'</span>';
      $creds .= '</a>';
      $creds .= '</div>';
      $creds .= '</div>';
      $creds .= '</div>';

      echo $creds;
	}
}



