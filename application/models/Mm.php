<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Mm extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();

    }

	

	function get($table, $data = array(), $returnformat = 'rear'){

        if(!empty($data['select']))

            $this->db->select($data['select'],false);

        $table = $this->db->dbprefix($table);

		$this->db->from($table);

        

        /*

        $data['join'] = array(

                            array('table1', 'table.a = table1.a', 'left|right'),

                            array('table2', 'table.b = table2.b', 'left|right')

                        );

        */

        if(!empty($data['join']) AND is_array($data['join'])){

            foreach($data['join'] as $jjoin){

                //$jjoin[0] = isset($jjoin[0]) ? $jjoin[0] : '';

                $jjoin[1] = isset($jjoin[1]) ? $jjoin[1] : '';

                $jjoin[2] = isset($jjoin[2]) ? $jjoin[2] : '';

                $this->db->join($jjoin[0], $jjoin[1], $jjoin[2]);

            }

        }

        

        /*

        $data['where'] = array(

                            'id' => 'idnya',

                            'nama !=' => 'namanya'

                        );

        $data['where'] = array(

                            array('id', 'idnya'),

                            array('alamat !=', 'alamatnya')

                        );

        $data['where'] = "id = 'idnya' OR nama = 'namanya'";

        */

		if( !empty($data['where']) ){

            if(!empty($data['where'][0]) AND is_array($data['where'])){

                foreach($data['where'] as $wwhere){

                    if(isset($wwhere[1]) AND $wwhere[1]){
						$this->db->where($wwhere[0], $wwhere[1]);
					} else {
						$this->db->where($wwhere[0]);
					}

                }

            } else if(is_array($data['where'])){

                foreach($data['where'] as $kwhere => $vwhere){

                    $this->db->where($kwhere, $vwhere);

                }

            } else {

                $this->db->where( $data['where'] );

            }

		}

		if( !empty($data['or_where']) ){

            if(isset($data['or_where'][0]) AND is_array($data['or_where'])){

                foreach($data['or_where'] as $wwhere){

                    $this->db->or_where($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['or_where'])){

                foreach($data['or_where'] as $kwhere => $vwhere){

                    $this->db->or_where($kwhere, $vwhere);

                }

            }

		}

        

        /*

        $data['where_in'] = array(

                            'id' => array(),

                            'alamat' => array()

                        );

        $data['where_in'] = array(

                            array('id', array())),

                            array('alamat', array()))

                        );

        */

		if( !empty($data['where_in']) ){

            if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                foreach($data['where_in'] as $wwhere){

                    $this->db->where_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['where_in'])){

                foreach($data['where_in'] as $kwhere => $vwhere){

                    $this->db->where_in($kwhere, $vwhere);

                }

            }

		}

		if( !empty($data['or_where_in']) ){

            foreach($data['or_where_in'] as $wwhere){

                $this->db->or_where_in($wwhere[0], $wwhere[1]);

            }

            if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                foreach($data['where_in'] as $wwhere){

                    $this->db->where_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['where_in'])){

                foreach($data['where_in'] as $kwhere => $vwhere){

                    $this->db->where_in($kwhere, $vwhere);

                }

            }

		}

		if( !empty($data['where_not_in']) ){

            if(isset($data['where_not_in'][0]) AND is_array($data['where_not_in'])){

                foreach($data['where_not_in'] as $wwhere){

                    $this->db->where_not_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['where_not_in'])){

                foreach($data['where_not_in'] as $kwhere => $vwhere){

                    $this->db->where_not_in($kwhere, $vwhere);

                }

            }

		}

		if( !empty($data['or_where_not_in']) ){

            if(isset($data['or_where_not_in'][0]) AND is_array($data['or_where_not_in'])){

                foreach($data['or_where_not_in'] as $wwhere){

                    $this->db->or_where_not_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['or_where_not_in'])){

                foreach($data['or_where_not_in'] as $kwhere => $vwhere){

                    $this->db->or_where_not_in($kwhere, $vwhere);

                }

            }

		}

        

        /*

        $data['like'] = array(

                            array('nama', 'namanya', 'before|after|both'),

                            array('alamat', 'alamatnya', 'before|after|both')

                        );

        */

        if(!empty($data['like']) AND is_array($data['like'])){

            foreach($data['like'] as $llike){

                //$llike[0] = isset($llike[0]) ? $llike[0] : '';

                $llike[1] = isset($llike[1]) ? $llike[1] : '';

                $llike[2] = isset($llike[2]) ? $llike[2] : '';

                $this->db->like($llike[0], $llike[1], $llike[2]);

            }

        }

        if(!empty($data['or_like']) AND is_array($data['or_like'])){

            foreach($data['or_like'] as $llike){

                //$llike[0] = isset($llike[0]) ? $llike[0] : '';

                $llike[1] = isset($llike[1]) ? $llike[1] : '';

                $llike[2] = isset($llike[2]) ? $llike[2] : '';

                $this->db->or_like($llike[0], $llike[1], $llike[2]);

            }

        }

			

		if( !empty($data['limit']) )

			$this->db->limit( $data['limit'], (!empty($data['offset']) ? $data['offset']: '' ));

        

        /*

        $data['order'] = "nama asc, alamat desc";

        $data['order'] = array(

                            array('nama', 'asc'),

                            array('alamat', 'desc')

                        );

        */

		if( !empty($data['order']) ){

            if(is_array($data['order'])){

                foreach($data['order'] as $oorder){

                    $this->db->order_by($oorder[0], $oorder[1]);

                }

            } else {

                $this->db->order_by( $data['order'] );

            }

		}
		
		
		
		//tambahan between 
		if( !empty($data['whbefr']) ){
            $this->db->where( 'created_at >=', $data['whbefr'] );
		}
		
		
		if( !empty($data['whbeto']) ){
            $this->db->where( 'created_at <=', $data['whbeto'] );
		}
		//tambahan 
			

		if (!empty($data['group_by']))

            $this->db->group_by($data['group_by']);

		$query	= $this->db->get();

        /*

        rear = result_array

        roar = row_array

        re = result

        ro = row

        */

        switch($returnformat){

            case 'rear' :

                return $query->result_array();

                break;

            case 'roar' :

                return $query->row_array();

                break;

            case 're' :

                return $query->result();

                break;

            case 'ro' :

                return $query->row();

                break;

        }
		

	}



	function count($table, $data = array()){

        $table = $this->db->dbprefix($table);

		$this->db->from($table);

        if(!empty($data['join']) AND is_array($data['join'])){

            foreach($data['join'] as $jjoin){

                //$jjoin[0] = isset($jjoin[0]) ? $jjoin[0] : '';

                $jjoin[1] = isset($jjoin[1]) ? $jjoin[1] : '';

                $jjoin[2] = isset($jjoin[2]) ? $jjoin[2] : '';

                $this->db->join($jjoin[0], $jjoin[1], $jjoin[2]);

            }

        }

		if( !empty($data['where']) ){

            if(!empty($data['where'][0]) AND is_array($data['where'])){

                foreach($data['where'] as $wwhere){

                    $this->db->where($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['where'])){

                foreach($data['where'] as $kwhere => $vwhere){

                    $this->db->where($kwhere, $vwhere);

                }

            } else {

                $this->db->where( $data['where'] );

            }

		}

		if( !empty($data['or_where']) ){

            if(isset($data['or_where'][0]) AND is_array($data['or_where'])){

                foreach($data['or_where'] as $wwhere){

                    $this->db->or_where($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['or_where'])){

                foreach($data['or_where'] as $kwhere => $vwhere){

                    $this->db->or_where($kwhere, $vwhere);

                }

            }

		}

		if( !empty($data['where_in']) ){

            if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                foreach($data['where_in'] as $wwhere){

                    $this->db->where_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['where_in'])){

                foreach($data['where_in'] as $kwhere => $vwhere){

                    $this->db->where_in($kwhere, $vwhere);

                }

            }

		}

		if( !empty($data['or_where_in']) ){

            foreach($data['or_where_in'] as $wwhere){

                $this->db->or_where_in($wwhere[0], $wwhere[1]);

            }

            if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                foreach($data['where_in'] as $wwhere){

                    $this->db->where_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['where_in'])){

                foreach($data['where_in'] as $kwhere => $vwhere){

                    $this->db->where_in($kwhere, $vwhere);

                }

            }

		}

		if( !empty($data['where_not_in']) ){

            if(isset($data['where_not_in'][0]) AND is_array($data['where_not_in'])){

                foreach($data['where_not_in'] as $wwhere){

                    $this->db->where_not_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['where_not_in'])){

                foreach($data['where_not_in'] as $kwhere => $vwhere){

                    $this->db->where_not_in($kwhere, $vwhere);

                }

            }

		}

		if( !empty($data['or_where_not_in']) ){

            if(isset($data['or_where_not_in'][0]) AND is_array($data['or_where_not_in'])){

                foreach($data['or_where_not_in'] as $wwhere){

                    $this->db->or_where_not_in($wwhere[0], $wwhere[1]);

                }

            } else if(is_array($data['or_where_not_in'])){

                foreach($data['or_where_not_in'] as $kwhere => $vwhere){

                    $this->db->or_where_not_in($kwhere, $vwhere);

                }

            }

		}

        if(!empty($data['like']) AND is_array($data['like'])){

            foreach($data['like'] as $llike){

                //$llike[0] = isset($llike[0]) ? $llike[0] : '';

                $llike[1] = isset($llike[1]) ? $llike[1] : '';

                $llike[2] = isset($llike[2]) ? $llike[2] : ''; //both, after, before

                $this->db->like($llike[0], $llike[1], $llike[2]);

            }

        }

        if(!empty($data['or_like']) AND is_array($data['or_like'])){

            foreach($data['or_like'] as $llike){

                //$llike[0] = isset($llike[0]) ? $llike[0] : '';

                $llike[1] = isset($llike[1]) ? $llike[1] : '';

                $llike[2] = isset($llike[2]) ? $llike[2] : '';

                $this->db->or_like($llike[0], $llike[1], $llike[2]);

            }
        }	
		
		//tambahan
		if( !empty($data['whbefr']) ){
            $this->db->where( 'created_at >=', $data['whbefr'] );
		}
		
		
		if( !empty($data['whbeto']) ){
            $this->db->where( 'created_at <=', $data['whbeto'] );
		}
		//tambahan
		
		return $this->db->count_all_results();

	}



    function save($table, $post, $data = array()){

        if(!empty($data)){

    		if( !empty($data['where']) ){

                if(!empty($data['where'][0]) AND is_array($data['where'])){

                    foreach($data['where'] as $wwhere){

                        $this->db->where($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where'])){

                    foreach($data['where'] as $kwhere => $vwhere){

                        $this->db->where($kwhere, $vwhere);

                    }

                } else {

                    $this->db->where( $data['where'] );

                }

    		}

    		if( !empty($data['or_where']) ){

                if(isset($data['or_where'][0]) AND is_array($data['or_where'])){

                    foreach($data['or_where'] as $wwhere){

                        $this->db->or_where($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['or_where'])){

                    foreach($data['or_where'] as $kwhere => $vwhere){

                        $this->db->or_where($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['where_in']) ){

                if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                    foreach($data['where_in'] as $wwhere){

                        $this->db->where_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where_in'])){

                    foreach($data['where_in'] as $kwhere => $vwhere){

                        $this->db->where_in($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['or_where_in']) ){

                if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                    foreach($data['where_in'] as $wwhere){

                        $this->db->where_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where_in'])){

                    foreach($data['where_in'] as $kwhere => $vwhere){

                        $this->db->where_in($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['where_not_in']) ){

                if(isset($data['where_not_in'][0]) AND is_array($data['where_not_in'])){

                    foreach($data['where_not_in'] as $wwhere){

                        $this->db->where_not_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where_not_in'])){

                    foreach($data['where_not_in'] as $kwhere => $vwhere){

                        $this->db->where_not_in($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['or_where_not_in']) ){

                if(isset($data['or_where_not_in'][0]) AND is_array($data['or_where_not_in'])){

                    foreach($data['or_where_not_in'] as $wwhere){

                        $this->db->or_where_not_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['or_where_not_in'])){

                    foreach($data['or_where_not_in'] as $kwhere => $vwhere){

                        $this->db->or_where_not_in($kwhere, $vwhere);

                    }

                }

    		}

            $table = $this->db->dbprefix($table);

            return $this->db->update($table, $post);

        } else {

            $table = $this->db->dbprefix($table);

    		$insert = $this->db->insert($table, $post);

            if( $return = $this->db->insert_id()){

                return $return;

            } else {

                return $insert;

            }

        }

    }



    function delete($table, $data = null){

        if(is_array($table)){

            $ntable = array();

            foreach($table as $vtable){

                $ntable[] = $this->db->dbprefix($vtable);

            }

            $table = $ntable;

        } else {

            $table = $this->db->dbprefix($table);

        }

        if(!empty($data)){

    		if( !empty($data['where']) ){

                if(!empty($data['where'][0]) AND is_array($data['where'])){

                    foreach($data['where'] as $wwhere){

                        $this->db->where($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where'])){

                    foreach($data['where'] as $kwhere => $vwhere){

                        $this->db->where($kwhere, $vwhere);

                    }

                } else {

                    $this->db->where( $data['where'] );

                }

    		}

    		if( !empty($data['or_where']) ){

                if(isset($data['or_where'][0]) AND is_array($data['or_where'])){

                    foreach($data['or_where'] as $wwhere){

                        $this->db->or_where($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['or_where'])){

                    foreach($data['or_where'] as $kwhere => $vwhere){

                        $this->db->or_where($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['where_in']) ){

                if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                    foreach($data['where_in'] as $wwhere){

                        $this->db->where_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where_in'])){

                    foreach($data['where_in'] as $kwhere => $vwhere){

                        $this->db->where_in($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['or_where_in']) ){

                if(isset($data['where_in'][0]) AND is_array($data['where_in'])){

                    foreach($data['where_in'] as $wwhere){

                        $this->db->where_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where_in'])){

                    foreach($data['where_in'] as $kwhere => $vwhere){

                        $this->db->where_in($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['where_not_in']) ){

                if(isset($data['where_not_in'][0]) AND is_array($data['where_not_in'])){

                    foreach($data['where_not_in'] as $wwhere){

                        $this->db->where_not_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['where_not_in'])){

                    foreach($data['where_not_in'] as $kwhere => $vwhere){

                        $this->db->where_not_in($kwhere, $vwhere);

                    }

                }

    		}

    		if( !empty($data['or_where_not_in']) ){

                if(isset($data['or_where_not_in'][0]) AND is_array($data['or_where_not_in'])){

                    foreach($data['or_where_not_in'] as $wwhere){

                        $this->db->or_where_not_in($wwhere[0], $wwhere[1]);

                    }

                } else if(is_array($data['or_where_not_in'])){

                    foreach($data['or_where_not_in'] as $kwhere => $vwhere){

                        $this->db->or_where_not_in($kwhere, $vwhere);

                    }

                }

    		}

        }

        return $this->db->delete($table);

    }



    function query($Q, $returnformat = 'rear'){

        $query = $this->db->query($Q);

        switch($returnformat){

            case 'rear' :

                return $query->result_array();

                break;

            case 'roar' :

                return $query->row_array();

                break;

            case 're' :

                return $query->result();

                break;

            case 'ro' :

                return $query->row();

                break;

        }

    }





   //tambahan hendrax

	function insert_batch($table,$data)
   {
      $this->db->insert_batch($table,$data);
	}

   function detail_pegawai($nipy)
   {
      $query = 'SELECT
                   sch_pegawai.nipy
                   , sch_pegawai.nama
                   , sch_pegawai.gelar_depan
                   , sch_pegawai.gelar_belakang
                   , sch_pegawai.tempat_lahir
                   , sch_pegawai.tanggal_lahir
                   , sch_pegawai.jenis_id_card
                   , sch_pegawai.nomor_id_card
                   , sch_pegawai.jenis_kelamin
                   , sch_pegawai.agama
                   , sch_pegawai.npwp
                   , sch_pegawai.status_pernikahan
                   , sch_pegawai.alamat_ktp
                   , sch_pegawai.desa_ktp_id
                   , sch_pegawai.kodepos_ktp
                   , sch_pegawai.alamat_rumah
                   , sch_pegawai.desa_rumah_id
                   , sch_pegawai.kodepos_rumah
                   , sch_pegawai.telpon
                   , sch_pegawai.telpon2
                   , sch_pegawai.email
                   , sch_pegawai.golongan_darah
                   , sch_pegawai.berat_badan
                   , sch_pegawai.tinggi_badan
                   , sch_pegawai.nidn
                   , sch_pegawai.tmt
                   , sch_pegawai.nomor_sk
                   , sch_pegawai.tanggal_sk
                   , sch_pegawai.tmt_80
                   , sch_pegawai.nomor_sk_80
                   , sch_pegawai.tanggal_sk_80
                   , sch_pegawai.golongan_awal
                   , sch_pegawai.jenis_bank
                   , sch_pegawai.nomor_rekening
                   , sch_pegawai.nama_rekening
                   , sch_pegawai.jenis_bank_pensiun
                   , sch_pegawai.nomor_rekening_pensiun
                   , sch_pegawai.nama_rekening_pensiun
                   , sch_pegawai.jumlah_tunjangan_anak
                   , sch_pegawai.jumlah_tunjangan_istri
                   , sch_pegawai.nomor_bpjs
                   , sch_pegawai.tanggal_pensiun
                   , sch_pegawai.berkala_berikutnya
                   , sch_pegawai.pangkat_berikutnya
                   , sch_pegawai.gaji_pokok
                   , sch_pegawai.ipk
                   , sch_pegawai.id_jenjang
                   , sch_pegawai.id_pangkat
                   , sch_pegawai.tmt_pangkat_terakhir
                   , sch_pegawai.mkg_pangkat_terakhir
                   , sch_pegawai.id_jabatan_struktural
                   , sch_pegawai.id_jabatan_fungsional
                   , sch_pegawai.id_unit
                   , sch_pegawai.id_bagian
                   , sch_pegawai.id_sub_bagian
                   , sch_pegawai.id_status_pegawai
                   , sch_pegawai.id_status_pekerjaan
                   , sch_pegawai.id_fungsi_pegawai
                   , sch_pegawai.id_bidang_kepakaran
                   , sch_pegawai.foto
                   , sch_dr_unit.induk
                   , sch_dr_unit.unit
                   , sch_dr_bagian.bagian
                   , sch_dr_sub_bagian.sub_bagian
                   , sch_dr_status_pekerjaan.status_pekerjaan
                   , sch_dr_status_pegawai.status_pegawai
                   , sch_dr_fungsi_pegawai.fungsi_pegawai
                   , sch_dr_jenis_pegawai.id_jenis_pegawai
                   , sch_dr_jenis_pegawai.jenis_pegawai
                   , sch_dr_pangkat.pangkat
                   , sch_dr_pangkat.golongan
                   , sch_dr_pangkat.urut_pangkat
                   , sch_dr_jenjang.jenjang
                   , sch_dr_jenjang.urut_jenjang
                   , sch_dr_bidang_kepakaran.bidang_kepakaran
                   , sch_dr_jabatan_struktural.jabatan_struktural
                   , sch_dr_jabatan_struktural.tunjangan as tunjangan_struktural
                   , sch_dr_jabatan_fungsional.jabatan_fungsional
                   , sch_dr_jabatan_fungsional.tunjangan as tunjangan_fungsional
                   , sch_kodepos.desa
                   , sch_kodepos.dt2
                   , sch_kodepos.kecamatan
                   , sch_kodepos.kabupaten
                   , sch_kodepos.propinsi
               FROM
                   sch_pegawai
                   LEFT JOIN sch_dr_unit
                       ON (sch_dr_unit.id_unit = sch_pegawai.id_unit)
                   LEFT JOIN sch_dr_sub_bagian
                       ON (sch_dr_sub_bagian.id_sub_bagian = sch_pegawai.id_sub_bagian)
                   LEFT JOIN sch_dr_status_pekerjaan
                       ON (sch_dr_status_pekerjaan.id_status_pekerjaan = sch_pegawai.id_status_pekerjaan)
                   LEFT JOIN sch_dr_status_pegawai
                       ON (sch_dr_status_pegawai.id_status_pegawai = sch_pegawai.id_status_pegawai)
                   LEFT JOIN sch_dr_pangkat
                       ON (sch_dr_pangkat.id_pangkat = sch_pegawai.id_pangkat)
                   LEFT JOIN sch_dr_jenjang
                       ON (sch_dr_jenjang.id_jenjang = sch_pegawai.id_jenjang)
                   LEFT JOIN sch_dr_fungsi_pegawai
                       ON (sch_dr_fungsi_pegawai.id_fungsi_pegawai = sch_pegawai.id_fungsi_pegawai)
                   LEFT JOIN sch_dr_jenis_pegawai
                       ON (sch_dr_jenis_pegawai.id_jenis_pegawai = sch_dr_fungsi_pegawai.id_jenis_pegawai)
                   LEFT JOIN sch_dr_jabatan_struktural
                       ON (sch_dr_jabatan_struktural.id_jabatan_struktural = sch_pegawai.id_jabatan_struktural)
                   LEFT JOIN sch_dr_jabatan_fungsional
                       ON (sch_dr_jabatan_fungsional.id_jabatan_fungsional = sch_pegawai.id_jabatan_fungsional)
                   LEFT JOIN sch_dr_bagian
                       ON (sch_dr_bagian.id_bagian = sch_pegawai.id_bagian)
                   LEFT JOIN sch_dr_bidang_kepakaran
                       ON (sch_dr_bidang_kepakaran.id_bidang_kepakaran = sch_pegawai.id_bidang_kepakaran)
                   LEFT JOIN sch_kodepos
                       ON (sch_kodepos.desaID = sch_pegawai.desa_ktp_id)
               WHERE nipy="'.$nipy.'"';

      $result = $this->db->query($query);

      return $result->row_array();
   }


    

    

 /***

 old pre

 heri.mardiansah@gmail.com

 ***/

	function insert( $table, $post = array() ) {

        $table = $this->db->dbprefix($table);

		$insert = $this->db->insert($table, $post);

        if( $return = $this->db->insert_id()){

            return $return;

        } else {

            return $insert;

        }

	}

    

    function insert_duplicate_update($table, $post, $update){

        $table = $this->db->dbprefix($table);

        $arrayKey = array_keys($post);

        $keys = implode(", ", $arrayKey);

        $values = implode("', '", $post);

        $q = "INSERT INTO `". $table ."` (". $keys .") VALUES ('". $values ."') ON DUPLICATE KEY UPDATE ". $update .";";

        $query = $this->db->query($q);

        return $query;

    }

    

    function insert_ignore($table, $post){

        $table = $this->db->dbprefix($table);

        $arrayKey = array_keys($post);

        $keys = implode(", ", $arrayKey);

        $values = implode("', '", $post);

        $q = "INSERT IGNORE INTO `". $table ."` (". $keys .") VALUES ('". $values ."')";

        $query = $this->db->query($q);

        return $query;

    }

    

    function insert_ignore_batch($table, $keys, $values){

        $table = $this->db->dbprefix($table);

        if(is_array($values)) $values = implode(", ", $values);

        $q = "INSERT IGNORE INTO `". $table ."` (". $keys .") VALUES ". $values ."";

        $query = $this->db->query($q);

        return $query;

    }

	

    function get_insert($table, $returnIndex, $where, $post = array()){

        $table = $this->db->dbprefix($table);

        if($data = $this->get_data($table, $where)){

            return $data[$returnIndex];

        } else {

            return $this->insert($table, $post);

        }

    }

        

    function run_query($query){

        return $this->db->query($query);

    }    

}

