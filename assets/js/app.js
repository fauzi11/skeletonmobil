$(document).ajaxStart(function() { 
    Pace.restart(); 
});

$(document).ready(function($) {
    $('.datatable').DataTable();
    $('.select2').select2();

    $('.submit_input').submit(function (event) {
        $('.btn-submit').attr('disabled', 'disabled');
        var form = this.closest('form');
        event.preventDefault();

        bootbox.confirm({
            message: 'Apakah anda yakin ?',
            buttons: {
                cancel: {
                    label: 'Tidak'
                },
                confirm: {
                    label: 'Ya'
                }
            },
            callback: function (result) {
                if (result) {
                    $('.form-control').removeAttr('disabled');
                    form.submit();
                } else {
                    $('.btn-submit').removeAttr('disabled');
                }
            }
        });
    });

    $(document).on('click', '.btn-delete', function(){
        var link = $(this).attr('data-link');

        bootbox.confirm({
            message: 'Apakah anda yakin ?',
            buttons: {
                cancel: {
                    label: 'Tidak'
                },
                confirm: {
                    label: 'Ya'
                }
            },
            callback: function (result) {
                if (result) {
                    window.location = link;
                } 
            }
        });
    });
});